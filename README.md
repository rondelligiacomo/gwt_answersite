If you like what I do consider to buy me a coffee ! https://www.buymeacoffee.com/rondelligiacomo


##Progetto di ingegneria del software 2018/2019 Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia

Questo e' il repository del progetto di ingegneria del software, il quale consiste nella creazione di un sito web che permetta agli utenti iscritti di porre domande e di fornire risposte agli interrogativi proposti.

L'applicazione permette all'utente di registrarsi al sito, loggarsi, inserire una nuova domanda, rispondere ad una domanda, visualizzare domande e risposte.

L'utente di tipo giudice puo' dare un giudizio ad una risposta data da un altro utente.

L'utente di tipo amministratore puo' creare e rimuovere categorie.

Nella repository sono presenti i codici sorgenti dell'applicazione web.