package com.google.gwt.sample.sweng;

import static org.mockito.Mockito.mock;



import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.gwt.sample.sweng.client.GreetingService;
import com.google.gwt.sample.sweng.server.GreetingServiceImpl;
import com.google.gwt.sample.sweng.shared.Categoria;
import com.google.gwt.sample.sweng.shared.Domanda;
import com.google.gwt.sample.sweng.shared.Risposta;

/**
 * Classe di test
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

class TestImpl {

	static GreetingServiceImpl implTest;

	/**
	 * Setup
	 */
	@BeforeAll
	public static void setup() {
		GreetingService interf = mock(GreetingService.class);
		implTest = new GreetingServiceImpl();
		implTest.setMockito(interf);		
	}

	/**
	 * Test inizializzazione
	 */
	@Test
	@DisplayName("Test di inizializzazione")
	public void testInit() {
		implTest.init();
	}


	/**
	 * Test per il Login
	 */
	@Test
	@DisplayName("Test di login")
	public void testLogin() {
		implTest.init();
		//utente non trovato
		assertEquals(0, implTest.logIn("utenteprova@mail.com", "pswprova") );
		assertEquals(2, implTest.logIn("root@root.com", "root") );

	}

	/**
	 * Test per la registrazione
	 */
	@Test
	@DisplayName("Test registrazione")
	public void testSignUp() {

		ArrayList<String> dati = new ArrayList<String>();
		dati.add("username");
		dati.add("password");
		dati.add("username@mail.com");
		dati.add("nome");
		dati.add("cognome");
		dati.add("M");
		dati.add("10/07/1992");
		dati.add("Bologna");
		dati.add("via ignota");

		ArrayList<String> datis = new ArrayList<String>(3);

		// registrazione ok: campi obbligatori
		assertEquals("Hai completato la registrazione", implTest.signUp(dati));

		// registrazione fallita: campi obbligatori non compilati
		dati.set(2, "");
		assertEquals("Bisogna riempire tutti i campi obbligatori", implTest.signUp(dati));

		// registrazione fallita: email gia' presente nel sistema
		dati.set(2, "username@mail.com");
		assertEquals("L'indirizzo email esiste", implTest.signUp(dati));

		// registrazione fallita: indirizzo email amministratore
		dati.set(2, "root@root.com");
		assertEquals("L'indirizzo email esiste", implTest.signUp(dati));
	}

	/**
	 * Test per NominaGiudice
	 */
	@Test
	@DisplayName("Test per nominare giudice")

	public void testNomina() {
		assertEquals("giudice correttamente nominato", implTest.nominaGiudice("username@mail.com"));
	}

	/**
	 * Test per aggiungi categoria
	 */
	@Test
	@DisplayName("Test generazione categoria")

	public void testNuovaCategoria() {

		assertTrue(implTest.nuovaCategoria(new Categoria ("Sport"), null));
		assertFalse(implTest.nuovaCategoria(new Categoria ("Sport"), null));
		assertFalse(implTest.nuovaCategoria(new Categoria (""), null));
	}

	/**
	 * Test per aggiungi sottocategoria
	 */
	@Test
	@DisplayName("Test nuova sottocategoria")

	public void testNuovaSottoCategoria() {

		assertTrue(implTest.nuovaCategoria(new Categoria ("Basket"), "Sport"));
		assertFalse(implTest.nuovaCategoria(new Categoria ("Sport"), "Sport"));
		assertFalse(implTest.nuovaCategoria(new Categoria (""), "Sport"));
	}

	/**
	 * Test per rinominare una categoria 
	 */
	@Test
	@DisplayName("Test rinominare categoria")

	public void testRinominaCategoria() {

		assertTrue(implTest.rinominaCategoria("Basket", "Basketball"));
		assertFalse(implTest.rinominaCategoria("Basket", ""));
	}

	/**
	 * Test per aggiungere una domanda
	 */
	@Test
	@DisplayName("Test nuova domanda")

	public void testNuovaDomanda() {

		ArrayList<String> link = new ArrayList<String>();
		assertTrue(implTest.inserisciDomanda(new Domanda(0, "Chi e' l'MVP 2019?", "Sport", "root@root.com", link)));
		assertTrue(implTest.inserisciDomanda(new Domanda(1, "Chi ha vinto la champions nel 2019?", "Sport", "root@root.com", link)));
		assertFalse(implTest.inserisciDomanda(new Domanda(2, "  ", "Sport", "root@root.com",link)));

	}


	/**
	 * Test per elimina domanda
	 */
	@Test
	@DisplayName("Test elimina domanda")

	public void testEliminaDomanda() {

		assertTrue(implTest.rimuoviDomanda(0));
		assertFalse(implTest.rimuoviDomanda(1000));
	}


	/**
	 * Test per aggiungere una risposta
	 */
	@Test
	@DisplayName("Test nuova risposta")
	public void testNuovaRisposta() {

		ArrayList<String> link = new ArrayList<String>();
		assertTrue(implTest.inserisciRisposta(new Risposta(0, 0, "Sono stato io", "root@root.com", link)));
		assertTrue(implTest.inserisciRisposta(new Risposta(0, 1, "E' stato Gabri", "root@root.com", link)));
		assertFalse(implTest.inserisciRisposta(new Risposta(0, 1, "  ", "root@root.com", link)));
	}

	/**
	 * Test per eliminare risposta
	 */
	@Test
	@DisplayName("Test elimina risposta")

	public void testEliminaRisposta() {

		assertTrue(implTest.rimuoviRisposta(0));
	}

}
