package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sample.sweng.shared.Categoria;
import com.google.gwt.sample.sweng.shared.Domanda;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;


/**
 * Classe che definisce  DomandeRisposte_Page
 * questa classe crea una tabella dove vengono inserite tutte le domande effettuate dagli utenti e le relative risposte
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

public class DomandeRisposte_Page {

	private VerticalPanel vPanel = null;
	private String filtroCategoria;
	private String selezione;
	private ArrayList<Categoria> sottocategorieFiltro = new ArrayList<Categoria>();
	private String percorso;

	/**
	 * Costruttore dell classe DomandeRisposte_Page
	 * @param vp VerticalPanel
	 */
	public DomandeRisposte_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * lancio la pagina con le domande e le risposte in una tabella
	 *
	 */
	public void onModuleLoad(final String filtroCategoria, final String percorso) {
        
		HTML title = new HTML("<br><br><h2> TUTTE LE DOMANDE E LE RISPOSTE </h2>");
		vPanel.add(title);
		vPanel.add(new HTML("</br>"));

		this.filtroCategoria = filtroCategoria;
		this.percorso = percorso;

		
		final Label labelF;

		// imposto un titolo
		if(this.filtroCategoria.equals(null)) labelF = new Label("CATEGORIA: elenco delle categorie");
		else labelF = new Label("CATEGORIA: "+ this.percorso);

		final HorizontalPanel h1 = new HorizontalPanel();
		h1.add(labelF);

		vPanel.add(new HTML ("<br><hr>"));
		vPanel.add(h1);

		final HorizontalPanel h2 = new HorizontalPanel();
		final Button aggiungiFiltro = new Button("aggiungi filtro");

		// categorie che vengono visualizzate
		final ListBox cat = new ListBox();

		final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		greetingService.getAllCategorie(new AsyncCallback<ArrayList<Categoria>>() {

			@Override
			public void onFailure(Throwable caught) {
				MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
			}

			@Override
			public void onSuccess(ArrayList<Categoria> response) {
				Categoria temp = null;
				if(filtroCategoria != null) temp = new Categoria(filtroCategoria);
				categorieFiglie(response, temp);

				for(Categoria c : sottocategorieFiltro) {
					cat.addItem(c.getNome());
				}

				// domande associate al filtro di ricerca
				greetingService.getDomande(new AsyncCallback<ArrayList<Domanda>>() {

					@Override
					public void onFailure(Throwable caught) {
						MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
					};

					@Override
					public void onSuccess(ArrayList<Domanda> response) {
						ArrayList<Domanda> domande = new ArrayList<Domanda>();

						for(Domanda d : response) {

							if(d.getCategoria().equals(filtroCategoria)) domande.add(d);

							else if (sottocategorieFiltro.size()>0) {
								h2.add(cat);
								h2.add(aggiungiFiltro);
								for(Categoria sottocategoria : sottocategorieFiltro) {
									if(d.getCategoria().equals(sottocategoria.getNome())) {
										domande.add(d);
										final HorizontalPanel h1 = new HorizontalPanel();

									}
								}
							}
						}

						//set tabella di visualizzazione delle domande e delle relative colonne
						// colonna testo
						CellTable<Domanda> tabellaDomande = new CellTable<Domanda>(50);

						TextColumn<Domanda> txtColTesto = new TextColumn<Domanda>() {
							@Override
							public String getValue(Domanda d) {
								return d.getTesto();
							}
						};
						tabellaDomande.addColumn(txtColTesto, "DOMANDA");

						//colonna autore
						TextColumn<Domanda> txtColAutore = new TextColumn<Domanda>() {
							@Override
							public String getValue(Domanda d) {
								return d.getNomeUtente();
							}
						};
						tabellaDomande.addColumn(txtColAutore, "AUTORE");

						//colonna data
						TextColumn<Domanda> txtColData = new TextColumn<Domanda>() {
							@Override
							public String getValue(Domanda d) {
								return d.getDay();
							}
						};
						tabellaDomande.addColumn(txtColData, "DATA");

						//colonna ora
						TextColumn<Domanda> txtColOra = new TextColumn<Domanda>() {
							@Override
							public String getValue(Domanda d) {
								return d.getTime();
							}
						};
						tabellaDomande.addColumn(txtColOra, "ORARIO");

						// set modalita' di selezione
						tabellaDomande.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
						final SingleSelectionModel<Domanda> selectionModel = new SingleSelectionModel<Domanda>();
						tabellaDomande.setSelectionModel(selectionModel);

						selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
							public void onSelectionChange(SelectionChangeEvent event) {
								Domanda scelta = selectionModel.getSelectedObject();
								if (scelta != null) {
									vPanel.clear();
									ElencoRisposte_Page er = new ElencoRisposte_Page(vPanel);
									er.onModuleLoad(scelta, percorso);
								}
							}
						});

						tabellaDomande.setRowCount(domande.size(), true);
						tabellaDomande.setRowData(0, domande);
						vPanel.add(tabellaDomande);	
					}

				});
			}

		});

		aggiungiFiltro.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				selezione = cat.getSelectedItemText();
				vPanel.clear();
				DomandeRisposte_Page dr = new DomandeRisposte_Page(vPanel);
				String percorsoCat = percorso + " -> " + selezione;
				dr.onModuleLoad(selezione, percorsoCat);
			}

		});

		Button noFiltri = new Button("Annulla filtri");
		noFiltri.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				vPanel.clear();
				DomandeRisposte_Page dr = new DomandeRisposte_Page(vPanel);
				dr.onModuleLoad(null, "");
			}

		});

		vPanel.add(h2);
		vPanel.add(noFiltri);
		vPanel.add(new HTML ("<br><hr>"));
	}

	/**
	 * Metodo per la visualizzazione delle categorie e relative sottocategorie
	 */
	private void categorieFiglie(ArrayList<Categoria> categorie, Categoria padre) {

		if(padre != null) {
			if(padre.getNome().equals("Base")) 	return;

			for(Categoria c : categorie) {
				if(c.getPadre().getNome().equals(padre.getNome())) {

					if(c.getPadre().getCategorieFiglie().size()>0) 	categorieFiglie(categorie, c);
					else categorieFiglie(categorie, new Categoria("Base"));
					sottocategorieFiltro.add(c);
				}
			}
		} 
		else {
			sottocategorieFiltro = categorie;
		}
	}
}