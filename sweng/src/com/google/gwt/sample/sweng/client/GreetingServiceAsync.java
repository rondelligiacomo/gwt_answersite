package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;


import com.google.gwt.sample.sweng.shared.Categoria;
import com.google.gwt.sample.sweng.shared.Domanda;
import com.google.gwt.sample.sweng.shared.Risposta;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {

	/**
	 * Metodo per creare e aggiungere l'admin al db e le categorie base
	 * @param callback 
	 * @throws IllegalArgumentException
	 */
	void init(AsyncCallback<Void> callback) throws IllegalArgumentException;

	/**
	 * Metodo per la registrazione dell'utente
	 * @param dati dell'utente che effettua la registrazione
	 * @param callback 
	 * @throws IllegalArgumentException
	 */
	void signUp(ArrayList<String> dati, AsyncCallback<String> callback) throws IllegalArgumentException;

	/**
	 * Metodo per il login di un utente registrato
	 * @param nomeUtente utente registrato
	 * @param password utente registrato
	 * @param callback 
	 * @throws IllegalArgumentException
	 */
	void logIn(String nomeUtente, String password, AsyncCallback<Integer> callback) throws IllegalArgumentException;

	/**
	 * Metodo per ottenere tutti gli utenti registrati alla piattaforma non promossi a giudice
	 * @param callback
	 * @return lista mail utenti registrati
	 * @throws IllegalArgumentException
	 */
	void getUtenti(AsyncCallback<ArrayList<String>> callback) throws IllegalArgumentException;

	/**
	 * Metodo che ritorna i dati di un utente registrato
	 * @param email dell'utente registrato -> RITORNA UNA STRINGA E NON UN ARRAY PERCHE?
	 * @param callback 
	 * @throws IllegalArgumentException
	 */
	void getDatiUtente(String email, AsyncCallback<String> callback)throws IllegalArgumentException;

	/**
	 * Metodo che ritorna la categoria associata all'id
	 * @param id categoria da ritornare
	 * @param callback
	 * @throws IllegalArgumentException
	 */
	void getCategoria(int id, AsyncCallback<Categoria> callback) throws IllegalArgumentException;

	/**
	 * Metodo che ritorna tutte le categorie presenti nel sistema
	 * @param callback 
	 * @throws IllegalArgumentException
	 */
	void getAllCategorie(AsyncCallback<ArrayList<Categoria>> callback) throws IllegalArgumentException;

	/**
	 * Metodo per l'inserimento di una categoria
	 * @param c Categoria 
	 * @param padre categoria
	 * @param callback
	 * @throws IllegalArgumentException
	 */
	void nuovaCategoria(Categoria c, String padre, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	/**
	 * Metodo per rinominare una categoria
	 * @param old, nome categoria da rinominare
	 * @param name, nuovo nome categoria
	 * @param callback
	 * @throws IllegalArgumentException
	 */
	void rinominaCategoria(String old, String newNome, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	/**
	 * Metodo che ritorna tutte le domande
	 * @param callcack 
	 * @throws IllegalArgumentException
	 */
	void getDomande(AsyncCallback<ArrayList<Domanda>> callback) throws IllegalArgumentException;
	
	/**
	 * Metodo l'inserimento di una nuova domanda
	 * @param datiDomanda, categoria testo e link relativi alla domanda
	 * @param emailUtente che inserisce la domanda
	 * @throws IllegalArgumentException

	 */
	void inserisciDomanda(Domanda d, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	
	/**
	 * Metodo per l'eliminazione di una domanda 
	 * @param domanda, testo della domanda che l'utente intende inserire 
	 * @return true se la domanda e' stata eliminata con successo, false altrimenti
	 * @throws IllegalArgumentException
	 */
	void rimuoviDomanda(int id, AsyncCallback<Boolean> callback) throws IllegalArgumentException;
	
	/**
	 * Metodo che ritorna tutte le risposte relative ad una domanda
	 * @param idDomanda
	 * @param callcack 
	 * @throws IllegalArgumentException
	 */
	void getRisposte(int idDomanda, AsyncCallback<ArrayList<Risposta>> callback) throws IllegalArgumentException;
	
	/**
	 * Metodo che ritorna tutte le risposte
	 * @param callcack 
	 * @throws IllegalArgumentException
	 */
	void getTutteRisposte(AsyncCallback<ArrayList<Risposta>> callback) throws IllegalArgumentException;
	
	/**
	 * Metodo l'inserimento di una nuova risposta
	 * @param datiRisposta, testo e link relativi alla risposta
	 * @param idDomanda relativa alla risposta
	 * @param emailUtente che inserisce la risposta
	 * @throws IllegalArgumentException

	 */
	void inserisciRisposta(Risposta r, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	/**
	 * Metodo per l'eliminazione di una risposta 
	 * @param id della risposta che l'utente intende inserire 
	 * @return true se la risposta e' stata eliminata con successo, false altrimenti
	 * @throws IllegalArgumentException
	 */
	void rimuoviRisposta(int id, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	/**
	 * Metodo per ordinare le risposte
	 * @param id
	 * @throws IllegalArgumentException
	 */
	void ordinaRisposte(int idDomanda, AsyncCallback<ArrayList<Risposta>> callback) throws IllegalArgumentException;

	/**
	 * Metodo per inserire un voto
	 * @param voto 
	 * @throws IllegalArgumentException
	 */
	void  inserisciVoto(Risposta r, String emailG, String v, AsyncCallback<Boolean> callback)throws IllegalArgumentException;

	/**
	 * Metodo che consente di nominare un giudice
	 * @param nomeUtente dell'utente registrato
	 * @param callcack 
	 * @throws IllegalArgumentException
	 */
	void nominaGiudice(String email, AsyncCallback<String> callback)throws IllegalArgumentException;

}
