package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;
import com.google.gwt.sample.sweng.shared.Categoria;
import com.google.gwt.sample.sweng.shared.Domanda;
import com.google.gwt.sample.sweng.shared.Risposta;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {




	/**
	 * Metodo per creare e aggiungere l'admin al db e le categorie base
	 * @throws IllegalArgumentException
	 */
	void init() throws IllegalArgumentException;

	/**
	 * Metodo per la registrazione dell'utente
	 * @param dati dell'utente che effettua la registrazione
	 * @return esito richiesta 
	 * @throws IllegalArgumentException
	 */
	
	String signUp(ArrayList<String> dati) throws IllegalArgumentException;

	
	/**
	 * Metodo per il login di un utente registrato
	 * @param nomeUtente utente registrato
	 * @param password utente registrato
	 * @return esito richiesta 
	 * @throws IllegalArgumentException
	 */
	int logIn(String nomeUtente, String password) throws IllegalArgumentException;

	/**
	 * Metodo per ottenere tutti gli utenti registrati alla piattaforma
	 * @return lista mail utenti registrati
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getUtenti() throws IllegalArgumentException;

	/**
	 * Metodo che ritorna i dati di un utente registrato
	 * @param email dell'utente registrato -> RITORNA UNA STRINGA E NON UN ARRAY PERCHE?
	 * @return informazioni dell'utente richiesto
	 * @throws IllegalArgumentException
	 */
	String getDatiUtente(String email)throws IllegalArgumentException;

	
	
	/**
	 * Metodo che ritorna la categoria associata all'id
	 * @param id categoria da ritornare
	 * @throws IllegalArgumentException
	 */
	Categoria getCategoria(int id) throws IllegalArgumentException;
	
	/**
	 * Metodo che ritorna tutte le categorie presenti nel sistema
	 * @throws IllegalArgumentException
	 */
	ArrayList<Categoria> getAllCategorie() throws IllegalArgumentException;
	
	/**
	 * Metodo per l'inserimento di una categoria
	 * @param c Categoria 
	 * @param padre categoria
	 * @throws IllegalArgumentException
	 */
	boolean nuovaCategoria(Categoria c, String padre) throws IllegalArgumentException;
	
	/**
	 * Metodo per rinominare una categoria
	 * @param old, nome categoria da rinominare
	 * @param name, nuovo nome categoria
	 * @param callback
	 * @throws IllegalArgumentException
	 */
	boolean rinominaCategoria(String old, String newNome) throws IllegalArgumentException;

	/**
	 * Metodo che ritorna tutte le domande
	 * @return domande presenti nel sistema 
	 * @throws IllegalArgumentException
	 */
	ArrayList<Domanda> getDomande() throws IllegalArgumentException;
	
	/**
	 * Metodo l'inserimento di una nuova domanda
	 * @param datiDomanda, categoria testo e link relativi alla domanda
	 * @param emailUtente che inserisce la domanda
	 * @return esito inserimento
	 */
	boolean inserisciDomanda(Domanda d) throws IllegalArgumentException;

	/**
	 * Metodo per l'eliminazione di una domanda 
	 * @param id della domanda da rimuovere
	 * @return true se la domanda e' stata eliminata con successo, false altrimenti
	 * @throws IllegalArgumentException
	 */
	boolean rimuoviDomanda(int id) throws IllegalArgumentException;

	/**
	 * Metodo che ritorna tutte le risposte relative ad una domanda
	 * @return risposte presenti nel db
	 * @throws IllegalArgumentException
	 */
	ArrayList<Risposta> getRisposte(int idDomanda) throws IllegalArgumentException;
	
	/**
	 * Metodo che ritorna tutte le risposte relative ad una domanda
	 * @return risposte presenti nel db
	 * @throws IllegalArgumentException
	 */
	ArrayList<Risposta> getTutteRisposte() throws IllegalArgumentException;
	
	
	/**
	 * Metodo l'inserimento di una nuova risposta
	 * @param risposta r da inserire
	 * @return esito inserimento
	 */
	boolean inserisciRisposta(Risposta r) throws IllegalArgumentException;

	/**
	 * Metodo per eliminare una risposta
	 * @param id della risposta da rimuovere
	 * @return esito operazione
	 * @throws IllegalArgumentException
	 */
	boolean rimuoviRisposta(int id) throws IllegalArgumentException;

	/**
	 * Metodo per ordinare le risposte
	 * @param id associato alle risposte relative a una domanda
	 * @return
	 * @throws IllegalArgumentException
	 */
	ArrayList<Risposta> ordinaRisposte (int idDomanda) throws IllegalArgumentException;
	
	/**
	 * Metodo per inserire un voto ad una risposta che non e' ancora stata votata
	 * @param oggetto r di tipo Risposta
	 * @param emailGiudice di tipo String
	 * @param voto di tipo int
	 * @return true se l'operazione viene eseguita con successo,
	 * 	false altrimenti
	 */
	boolean inserisciVoto(Risposta r, String emailG, String v) throws IllegalArgumentException;

	/**
	 * Metodo che consente di nominare un giudice
	 * @param nomeUtente dell'utente registrato
	 * @return esito della richiesta
	 * @throws IllegalArgumentException
	 */
	String nominaGiudice(String email)throws IllegalArgumentException;


}
