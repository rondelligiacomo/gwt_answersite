package com.google.gwt.sample.sweng.client;

import com.google.gwt.core.client.EntryPoint;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;


/**
 * Questa classe carica il profilo con i dati dell'utente di tipo amministratore
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */


public class Amministratore_Page implements EntryPoint {

	private VerticalPanel vPanel = null;

	/**
	 * Costruttore della classe Amministratore_Page
	 * @param vp Vertical Panel
	 */
	public Amministratore_Page( VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Caricamento di Amministratore_Page
	 */
	public void onModuleLoad() {

		Menu menu = new Menu( this.vPanel, Account.tipoAccount);
		menu.onModuleLoad();



		vPanel.add(new HTML("<br>"));
		vPanel.add(new HTML("<br>"));
		vPanel.add(new HTML("<br>"));
		final HorizontalPanel hPanel = new HorizontalPanel();

		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getDatiUtente(Account.email, new AsyncCallback<String>() {

				public void onFailure(Throwable caught) {
					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText(caught.getMessage());
				}

				public void onSuccess(String info) {
					String [] arrayInfo = info.split("\n");
					

					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					String htmlProfilo = "<h1>Il tuo profilo</h1>"+
							arrayInfo[0]+"<br>"+
							arrayInfo[1]+"<br>"+
							arrayInfo[2]+"<br>"+
							arrayInfo[3]+"<br>"+
							arrayInfo[4]+"<br>"+
							arrayInfo[5]+"<br>"+
							arrayInfo[6]+"<br>"+
							arrayInfo[7]+"<br>"+
							"Tipo profilo : Amministratore";
					

					hPanel.add(new HTML(htmlProfilo));

					vPanel.getElement().setAttribute("align", "center");
					vPanel.add(hPanel);
				}
			});
		}
		catch(Error e){};
	}

}
