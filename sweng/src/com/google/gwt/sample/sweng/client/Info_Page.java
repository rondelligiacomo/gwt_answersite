package com.google.gwt.sample.sweng.client;

import com.google.gwt.core.client.EntryPoint;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe che definisce Info_Page
 * questa pagina visualizza tutte le funzionalita' del sito di domande e risposte
 * le funzionalita' sono tratte dalle specifiche di progetto
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class Info_Page implements EntryPoint {

	private VerticalPanel vPanel = null;

	/**
	 * Costruttore della classe Info_Page
	 * @param vp VerticalPanel
	 */
	public Info_Page( VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Caricamento Page
	 */
	@Override
	public void onModuleLoad() {

		HorizontalPanel hPanel = new HorizontalPanel();

		HTML titolo = new HTML("<h1> Informazioni sul sito di domande e risposte </h1>");
		HTML descrizione = new HTML("<p> Questo sito e' stato progettato per permettere agli utenti che\n" + 
				"iscritti di porre\n" + 
				"domande e di fornire risposte agli\n" + 
				"interrogativi proposti.<br>" + "<br/>" +
				"PRINCIPALI FUNZIONALITA':<br><br>" + 
				"- Registrarsi al sito<br>" + 
				"- Inserire una nuova domanda<br>" + 
				"- Rispondere a una domanda<br>" + 
				"- Visualizzare domande e risposte <br>" +
				"- Dare un giudizio ad una risposta data da un altro utente (funzionalita' disponibile solo per i giudici</p>");

		

		vPanel.add(titolo);
		vPanel.add(new HTML("<br/>"));
		hPanel.add(descrizione);

		vPanel.add(hPanel);

		
		
		hPanel.getElement().setAttribute("align", "center");
		vPanel.getElement().setAttribute("align", "center");

	}
}