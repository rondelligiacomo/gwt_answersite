package com.google.gwt.sample.sweng.client;

import com.google.gwt.user.client.ui.MenuBar;

import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe che definisce il menu
 * in base al tipo di utente fa visualizzare piu' o meno funzionalita' 
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class Menu {

	private VerticalPanel vPanel = null;
	private int tipo = -1;

	/**
	 * Costruttore della classe Menu
	 * @param vp VerticalPanel
	 * @param tipo tipo di menu da generare
	 */
	public Menu(VerticalPanel vp, int tipo) {
		this.vPanel = vp;
		this.tipo = tipo;
	}

	/**
	 * Carica il Menu
	 */
	public void onModuleLoad() {

		Comando cmd = new Comando(this.vPanel);

		MenuBar menu = new MenuBar();

		// Utente non registrato
		if(this.tipo == 0) {
			
			
			MenuBar MenuCustom = new MenuBar(true);
		    MenuCustom.addItem("Login",cmd.comando("login"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Registrazione",cmd.comando("registrazione"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Visualizza domande",cmd.comando("Elenco Domande"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Info",cmd.comando("Info"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Membri Progetto",cmd.comando("members"));
			menu.addItem("Menu", MenuCustom);

		}
		// Giudice: 1
		
		if((this.tipo == 1)) {
			MenuBar MenuCustom = new MenuBar(true);
			MenuCustom.addItem("Profilo",cmd.comando("Profilo"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Logout",cmd.comando("logout"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Info",cmd.comando("info"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Inserisci domanda", cmd.comando("domanda"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Inserisci risposta", cmd.comando("risposta"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Visualizza tutte le domande", cmd.comando("Elenco Domande"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Cancella risposta", cmd.comando("elimina risposta")); 
			MenuCustom.addSeparator();
			MenuCustom.addItem("Membri Progetto",cmd.comando("members"));
			menu.addItem("Menu", MenuCustom);
		} 

		// Amministratore: 2
			if(this.tipo == 2 ) {
				MenuBar MenuCustom = new MenuBar(true);
				MenuCustom.addItem("Profilo",cmd.comando("Profilo"));
				MenuCustom.addSeparator();
				MenuCustom.addItem("Elenco domande",cmd.comando("Elenco Domande"));
				MenuCustom.addSeparator();
				MenuCustom.addItem("Logout",cmd.comando("logout"));
				MenuCustom.addSeparator();
				MenuCustom.addItem("Info",cmd.comando("info"));				 
				MenuCustom.addSeparator();
				MenuCustom.addItem("Cancella domanda", cmd.comando("elimina domanda"));
				MenuCustom.addSeparator();
				MenuCustom.addItem("Cancella risposta", cmd.comando("elimina risposta"));
				MenuCustom.addSeparator();
				MenuCustom.addItem("Gestione categorie", cmd.comando("gestione categorie")); 
				MenuCustom.addSeparator();
				MenuCustom.addItem("Nomina giudice", cmd.comando("Nomina giudice")); 
				MenuCustom.addSeparator();
				MenuCustom.addItem("Membri Progetto",cmd.comando("members"));
				menu.addItem("Menu", MenuCustom);
			}
		
		/*
		 * Utente registrato: 3
		 */
		if (this.tipo == 3) {
			MenuBar MenuCustom = new MenuBar(true);
			MenuCustom.addItem("Profilo",cmd.comando("Profilo"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Elenco domande",cmd.comando("Elenco Domande"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Inserisci domanda",cmd.comando("domanda"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Inserisci risposta",cmd.comando("risposta"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Info",cmd.comando("info"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Membri Progetto",cmd.comando("members"));
			MenuCustom.addSeparator();
			MenuCustom.addItem("Logout",cmd.comando("logout"));
			menu.addItem("Menu", MenuCustom);


		}
		menu.setSize("120em", "2.5em");
		RootPanel.get().add(menu, 0, 0);
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

}