package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;


import com.google.gwt.core.client.GWT;
import com.google.gwt.sample.sweng.shared.Risposta;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * Classe che definisce della EliminaRisposta_Page
 *questa classe gestisce l'eliminazione di una risposta dall'elenco delle risposte
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

public class EliminaRisposta_Page {

	private VerticalPanel vPanel = null;

	public EliminaRisposta_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	public void onModuleLoad() {

		HTML titolo = new HTML("<h1> Elimina risposta </h1>");
		vPanel.add(titolo);
		HTML titolo2 = new HTML("<h2> Seleziona la risposta che vuoi eliminare e verra' fatto </h2>");
		vPanel.add(titolo2);
	

		vPanel.add(new HTML("<br/>")); 	

		// risposte presenti nel sistema
		final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

		greetingService.getTutteRisposte(new AsyncCallback<ArrayList<Risposta>>() {

			@Override
			public void onFailure(Throwable caught) {
				final MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
			};

			public void onSuccess(ArrayList<Risposta> response) {

				//set tabella di visualizzazione di tutte le risposte presenti
				// colonna testo
				CellTable<Risposta> tabellaRisposte = new CellTable<Risposta>(50);

				TextColumn<Risposta> txtColTesto = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						return r.getTesto();
					}
				};
				tabellaRisposte.addColumn(txtColTesto, "RISPOSTA");

				//colonna autore
				TextColumn<Risposta> txtColAutore = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						return r.getNomeUtente();
					}
				};
				tabellaRisposte.addColumn(txtColAutore, "AUTORE");

				//colonna data
				TextColumn<Risposta> txtColData = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						return r.getDay();
					}
				};
				tabellaRisposte.addColumn(txtColData, "DATA");

				//colonna ora
				TextColumn<Risposta> txtColOra = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						return r.getTime();
					}
				};
				tabellaRisposte.addColumn(txtColOra, "ORARIO");

				// set modalita' di selezione
				tabellaRisposte.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
				final SingleSelectionModel<Risposta> selectionModel = new SingleSelectionModel<Risposta>();
				tabellaRisposte.setSelectionModel(selectionModel);

				selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						Risposta scelta = selectionModel.getSelectedObject();
						if (scelta != null) {
							final int id = scelta.getId();
							try {
								greetingService.rimuoviRisposta(id, new AsyncCallback<Boolean>() {

									public void onFailure(Throwable caught) {
										MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
									}

									@Override
									public void onSuccess(Boolean response) {
										if(response) {
											MyDialogBox myDialogBox = new MyDialogBox("Risposta eliminata con successo");
											vPanel.clear();
											EliminaRisposta_Page ed = new EliminaRisposta_Page(vPanel);
											ed.onModuleLoad();
										}
										else {
											MyDialogBox myDialogBox = new MyDialogBox("Impossibile eliminare risposta");
										}
									}
								});
							} catch(Error e){};
						}
					}
				});

				tabellaRisposte.setRowCount(response.size(), true);
				tabellaRisposte.setRowData(0, response);
				vPanel.add(tabellaRisposte);	

			}
		});
	}
}
