package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.sample.sweng.shared.Categoria;
import com.google.gwt.sample.sweng.shared.Domanda;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe che definisce InserisciDomanda_Page
 * la classe gestisce l'inserimento di una nuova domanda nel sistema
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class InserisciDomanda_Page {

	private VerticalPanel vPanel = null;
	private VerticalPanel vtemp = null;

	private Button bInserisciDomanda;
	private TextArea testoDomanda; 
	private Label labelSelectedCategoria;
	protected ArrayList<Categoria> listaCategorie = new ArrayList<Categoria>();
	private TreeItem radice = new TreeItem();

	/**
	 * Costruttore dell classe InserisciDomanda_Page
	 * @param vp VerticalPanel
	 */
	public InserisciDomanda_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Caricamento Page con le varie funzionalita'
	 */
	public void onModuleLoad() {
		
		vtemp = new VerticalPanel();

		Menu menu = new Menu(this.vPanel, Account.tipoAccount);
		menu.onModuleLoad();

		HTML titolo = new HTML("<h1> Inserisci una nuova domanda </h1>");

		vPanel.add(titolo);
		vPanel.add(new HTML("</br>"));

		final Tree albero = new Tree();

		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
			greetingService.getAllCategorie(new AsyncCallback<ArrayList<Categoria>>() {

				public void onFailure(Throwable caught) {
					MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
				}

				@Override
				public void onSuccess(ArrayList<Categoria> response) {
					Categoria categoriaNull = new Categoria("null");
					listaCategorie.add(categoriaNull);
					//risoluzione albero delle categorie
					getAlbero(categoriaNull, response, radice);
					radice.setState(true);
					albero.addItem(radice);

				}
			});
		} catch(Error e){};

		albero.addSelectionHandler(new SelectionHandler<TreeItem>() {

			@Override
			public void onSelection(final SelectionEvent<TreeItem> event) {
				vtemp.clear();
				if(!event.getSelectedItem().getText().equals("ELENCO CATEGORIE")) {

					vtemp.add(new HTML("<br><hr>")); //riga di commento

					testoDomanda = new TextArea();
					testoDomanda.getElement().setAttribute("maxlength", "300");

					final Label label1 = new Label("Link 1: ");
					final TextBox link1 = new TextBox();
					final Label label2 = new Label("Link 2: ");
					final TextBox link2 = new TextBox();
					final Label label3 = new Label("Link 3: ");
					final TextBox link3 = new TextBox();
					final Label label4 = new Label("Link 4: ");
					final TextBox link4 = new TextBox();

					final Grid grid = new Grid(4,2);

					grid.setWidget(0, 0,  label1);
					grid.setWidget(0, 1, link1);
					grid.setWidget(1, 0,  label2);
					grid.setWidget(1, 1, link2);
					grid.setWidget(2, 0,  label3);
					grid.setWidget(2, 1, link3);
					grid.setWidget(3, 0,  label4);
					grid.setWidget(3, 1, link4);

					bInserisciDomanda = new Button("Aggiungi");

					labelSelectedCategoria = new Label("Categoria selezionata: " + event.getSelectedItem().getText());

					HorizontalPanel h1 = new HorizontalPanel();
					h1.add(testoDomanda);

					HorizontalPanel h3 = new HorizontalPanel();
					h3.add(grid);

					HorizontalPanel h2 = new HorizontalPanel();
					h2.add(bInserisciDomanda);
					h2.getElement().setAttribute("align", "center");

				
					// aggiungi domanda
					bInserisciDomanda.addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent eventClick) {
							//array link
							ArrayList<String> listaLink = new ArrayList<String>();
							listaLink.add(link1.getValue());
							listaLink.add(link2.getValue());
							listaLink.add(link3.getValue());
							listaLink.add(link4.getValue());
							
							int id = 1;
							
							Domanda nuova = new Domanda(id, testoDomanda.getValue(), event.getSelectedItem().getText(), Account.email, listaLink);

							final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
							greetingService.inserisciDomanda(nuova, new AsyncCallback<Boolean>() {

								@Override
								public void onFailure(Throwable caught) {
									MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
								}

								@Override
								public void onSuccess(Boolean response) {
									if (response) {
										final MyDialogBox myDialogBox = new MyDialogBox("Domanda inserita con successo");
										vPanel.clear();
										InserisciDomanda_Page insDom = new InserisciDomanda_Page(vPanel);
										insDom.onModuleLoad();
									}
									else {
										final MyDialogBox myDialogBox = new MyDialogBox("Impossibile inserire domanda");
									}
								}
							});

						}
					});

					vtemp.add(new HTML("<br/>"));
					vtemp.add(labelSelectedCategoria);	
					vtemp.add(new HTML("<br/>"));
					Label label = new Label("Inserisci il testo della domanda: ");
					vtemp.add(label);
					vtemp.add(new HTML("<br/>"));
					vtemp.add(h1);
					vtemp.add(new HTML("<br/>"));
					vtemp.add(h3);
					vtemp.add(new HTML("<br/>"));
					vtemp.add(h2);
					vtemp.getElement().setAttribute("align", "center");
				}
			}

		});
		HorizontalPanel hp = new HorizontalPanel();
		hp.add(albero);
		vPanel.add(hp);
		vPanel.add(vtemp);
	}

	/**
	 * Metodo per la risoluzione dell'albero delle categorie
	 * 
	 * @param padre di tipo Categoria
	 * @param categorie ArrayList<Categoria> di categorie prensenti nel sistema
	 * @param nodoPadre albero
	 */
	private void getAlbero(Categoria padre, ArrayList<Categoria> categorie, TreeItem nodoPadre) {

		// set nodo padre
		if(padre.getNome().equals("null")) nodoPadre.setText("ELENCO CATEGORIE");

		// set struttura albero
		for(Categoria c : categorie) {
			if(!listaCategorie.contains(c) && c.getPadre().getNome().equals(padre.getNome())) {
				TreeItem nodo = new TreeItem();
				nodo.setText(c.getNome());
				listaCategorie.add(c);

				for(Categoria sottoC : categorie) {
					if(sottoC.getPadre().getNome().equals(c.getNome())) {
						getAlbero(c, categorie, nodo);
					}
				}
				nodoPadre.addItem(nodo);
			}
		}
	}

}