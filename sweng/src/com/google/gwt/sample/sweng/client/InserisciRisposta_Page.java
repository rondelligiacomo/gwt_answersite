package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sample.sweng.shared.Domanda;
import com.google.gwt.sample.sweng.shared.Risposta;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * Classe per la definizione della InserisciRisposta_Page
 * permette di inserire una risposta ad una domanda fatta
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class InserisciRisposta_Page {

	private VerticalPanel vPanel = null;
	private VerticalPanel vtemp = null;

	private Button bInserisciRisposta;
	private TextArea testoRisposta; 

	/**
	 * Costruttore dell classe InserisciRisposta_Page
	 * @param vp VerticalPanel
	 */
	public InserisciRisposta_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	public void onModuleLoad() {
		
		vtemp = new VerticalPanel();

		HTML titolo = new HTML("<h1> Inserisci risposta </h1>");
		vPanel.add(titolo);
		vPanel.add(new HTML("<br/>"));
		HTML stitolo = new HTML("<h2> Clicca sulla domanda a cui vuoi inserire una risposta </h2>");
		vPanel.add(stitolo);
		vPanel.add(new HTML("<br/>"));

		final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		greetingService.getDomande(new AsyncCallback<ArrayList<Domanda>>() {

			@Override
			public void onFailure(Throwable caught) {
				MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
			};

			@Override
			public void onSuccess(ArrayList<Domanda> response) {

				//set tabella di visualizzazione delle domande e delle relative colonne
				// colonna testo
				CellTable<Domanda> tabellaDomande = new CellTable<Domanda>(50);

				TextColumn<Domanda> txtColTesto = new TextColumn<Domanda>() {
					@Override
					public String getValue(Domanda d) {
						return d.getTesto();
					}
				};
				tabellaDomande.addColumn(txtColTesto, "DOMANDA");

				//colonna autore
				TextColumn<Domanda> txtColAutore = new TextColumn<Domanda>() {
					@Override
					public String getValue(Domanda d) {
						return d.getNomeUtente();
					}
				};
				tabellaDomande.addColumn(txtColAutore, "AUTORE");

				//colonna data
				TextColumn<Domanda> txtColData = new TextColumn<Domanda>() {
					@Override
					public String getValue(Domanda d) {
						return d.getDay();
					}
				};
				tabellaDomande.addColumn(txtColData, "DATA");

				//colonna ora
				TextColumn<Domanda> txtColOra = new TextColumn<Domanda>() {
					@Override
					public String getValue(Domanda d) {
						return d.getTime();
					}
				};
				tabellaDomande.addColumn(txtColOra, "ORARIO");

				// set modalita' di selezione
				tabellaDomande.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
				final SingleSelectionModel<Domanda> selectionModel = new SingleSelectionModel<Domanda>();
				tabellaDomande.setSelectionModel(selectionModel);

				selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						
						vtemp.clear();
						Domanda scelta = selectionModel.getSelectedObject();
						if (scelta != null) {
							
							final int idDom = scelta.getId();
							final int id = 1;

							//campi form di inserimento risposta
							testoRisposta = new TextArea();
							testoRisposta.getElement().setAttribute("maxlength", "500");

							bInserisciRisposta = new Button("Aggiungi");

							HorizontalPanel h1 = new HorizontalPanel();
							h1.add(testoRisposta);
							h1.getElement().setAttribute("align", "center");
	
							final Label label1 = new Label("Link 1: ");
							final TextBox link1 = new TextBox();
							final Label label2 = new Label("Link 2: ");
							final TextBox link2 = new TextBox();
							final Label label3 = new Label("Link 3: ");
							final TextBox link3 = new TextBox();
							final Label label4 = new Label("Link 4: ");
							final TextBox link4 = new TextBox();

							final Grid grid = new Grid(4,2);
							
							grid.setWidget(0, 0,  label1);
							grid.setWidget(0, 1, link1);
							grid.setWidget(1, 0,  label2);
							grid.setWidget(1, 1, link2);
							grid.setWidget(2, 0,  label3);
							grid.setWidget(2, 1, link3);
							grid.setWidget(3, 0,  label4);
							grid.setWidget(3, 1, link4);
							
							HorizontalPanel h3 = new HorizontalPanel();
							h3.add(grid);
							
							HorizontalPanel h2 = new HorizontalPanel();
							h2.add(bInserisciRisposta);
							h2.getElement().setAttribute("align", "center");

							// aggiungi risposta
							bInserisciRisposta.addClickHandler(new ClickHandler() {

								@Override
								public void onClick(ClickEvent eventClick) {
									//array link
									ArrayList<String> listaLink = new ArrayList<String>();
									listaLink.add(link1.getValue());
									listaLink.add(link2.getValue());
									listaLink.add(link3.getValue());
									listaLink.add(link4.getValue());
									
									Risposta nuova = new Risposta(id, idDom, testoRisposta.getValue(), Account.email, listaLink);
									final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
									greetingService.inserisciRisposta(nuova, new AsyncCallback<Boolean>() {

										@Override
										public void onFailure(Throwable caught) {
											MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
										}

										@Override
										public void onSuccess(Boolean response) {
											if (response) {
												final MyDialogBox myDialogBox = new MyDialogBox("Risposta inserita con successo");
												vPanel.clear();
												InserisciRisposta_Page insRis = new InserisciRisposta_Page(vPanel);
												insRis.onModuleLoad();
											}
											else {
												final MyDialogBox myDialogBox = new MyDialogBox("Inserie testo risposta");
											}
										}
									});

								}
							});
							
							vtemp.add(new HTML("<br/>"));
							Label label = new Label("Inserisci la tua risposta: ");
							vtemp.add(label);
							vtemp.add(h1);
							vtemp.add(new HTML("<br/>"));
							vtemp.add(h3);
							vtemp.add(new HTML("<br/>"));
							vtemp.add(h2);
							
						}
					}
				});

				tabellaDomande.setRowCount(response.size(), true);
				tabellaDomande.setRowData(0, response);
				vPanel.add(tabellaDomande);	
				vPanel.add(vtemp);
			}

		});

		vPanel.add(new HTML ("<br><hr>"));

	}

}
