package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.sample.sweng.shared.Categoria;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;

import com.google.gwt.sample.sweng.client.MyDialogBox;

/**
 * Classe che definisce GestioneCategorie_Page
 * Questa classe permette all'utente amministratore di aggiungere o modificare le cateogorie presenti nel sistema
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class GestioneCategorie_Page {

	private VerticalPanel vPanel = null;
	private VerticalPanel vtemp = null;

	private Button bAggiungiCategoria;
	private TextBox tAggiungiCategoria;

	private Button bAggiungiSottoCategoria;
	private TextBox tAggiungiSottoCategoria;

	private Button bModificaCategoria;
	private TextBox tModificaCategoria;

	private Label labelSelectedCategoria;

	protected ArrayList<Categoria> listaCategorie = new ArrayList<Categoria>();
	private TreeItem radice = new TreeItem();

	/**
	 * Costruttore della classe GestioneCategorie_Page
	 * @param vp VerticalPanel
	 */
	public GestioneCategorie_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Caricamento Page con le varie funzionalita'
	 */
	public void onModuleLoad() {
		
		vtemp = new VerticalPanel();

		Menu menu = new Menu(this.vPanel, Account.tipoAccount);
		menu.onModuleLoad();

		HTML titolo = new HTML("<h1> Gestione Categorie </h1>");
		vPanel.add(titolo);
		vPanel.add(new HTML("</br>"));

		//Nuova categoria
		HTML stitolo1 = new HTML("<h2> Nuova Categoria </h2>");
		vPanel.add(stitolo1);
		vPanel.add(new HTML("</br>"));

		bAggiungiCategoria = new Button("Nuova categoria base");
		tAggiungiCategoria = new TextBox();

		final HorizontalPanel h1 = new HorizontalPanel();
		final Grid grid = new Grid(2,2);

		grid.setWidget(0, 0,  tAggiungiCategoria);
		grid.setWidget(0, 1, bAggiungiCategoria);
		h1.add(grid);
		
		bAggiungiCategoria.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Categoria nuova = new Categoria(tAggiungiCategoria.getValue());

				try {
					final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
					greetingService.nuovaCategoria(nuova, null, new AsyncCallback<Boolean>() {

						public void onFailure(Throwable caught) {
							MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
						}

						@Override
						public void onSuccess(Boolean response) {
							if (response) {
								final MyDialogBox myDialogBox = new MyDialogBox("Categoria inserita con successo");
								vPanel.clear();
								GestioneCategorie_Page gc = new GestioneCategorie_Page(vPanel);
								gc.onModuleLoad();
							}
							else {
								final MyDialogBox myDialogBox = new MyDialogBox("Impossibile inserire categoria: la categoria esiste gia' oppure il testo non e' valido");
							}
						}
					});
				} catch(Error e){};
			}
		});

		vPanel.add(h1);

		final Tree albero = new Tree();

		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
			greetingService.getAllCategorie(new AsyncCallback<ArrayList<Categoria>>() {

				@Override
				public void onFailure(Throwable caught) {
					MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
				}

				@Override
				public void onSuccess(ArrayList<Categoria> response) {
					Categoria categoriaNull = new Categoria("null");
					listaCategorie.add(categoriaNull);
					//risoluzione albero delle categorie
					getAlbero(categoriaNull, response, radice);
					radice.setState(true);
					albero.addItem(radice);

				}
			});
		} catch(Error e){};

		// Aggiungi sottocategoria e modifica categorie
		vtemp.add(new HTML ("<br><hr>"));
		HTML stitolo3 = new HTML("<h2> Seleziona una categoria per aggiungere una sottocategoria o modificarne il nome</h2>");
		vtemp.add(stitolo3);
		vtemp.add(new HTML("</br>"));
		
		albero.addSelectionHandler(new SelectionHandler<TreeItem>() {

			@Override
			public void onSelection(final SelectionEvent<TreeItem> event) {
				vtemp.clear();

				if(!event.getSelectedItem().getText().equals("CATEGORIE DISPONIBILI")) {
					bAggiungiSottoCategoria = new Button("Aggiungi sottocategoria");
					tAggiungiSottoCategoria = new TextBox();
					labelSelectedCategoria = new Label("Selezione: " + event.getSelectedItem().getText());

					// mettere una bella griglia VIRGI
					bModificaCategoria = new Button("Modifica categoria");
					tModificaCategoria = new TextBox();
					tModificaCategoria.setText(event.getSelectedItem().getText());

					HorizontalPanel h2 = new HorizontalPanel();
					h2.add(tAggiungiSottoCategoria);
					h2.add(bAggiungiSottoCategoria);

					// aggiungi sottoCategoria
					bAggiungiSottoCategoria.addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent eventClick) {
							Categoria nuova = new Categoria(tAggiungiSottoCategoria.getValue());

							final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
							greetingService.nuovaCategoria(nuova, event.getSelectedItem().getText(), new AsyncCallback<Boolean>() {

								@Override
								public void onFailure(Throwable caught) {
									MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
								}

								@Override
								public void onSuccess(Boolean response) {
									if (response) {
										final MyDialogBox myDialogBox = new MyDialogBox("Sottocategoria inserita con successo");
										vPanel.clear();
										GestioneCategorie_Page gc = new GestioneCategorie_Page(vPanel);
										gc.onModuleLoad();
									}
									else {
										final MyDialogBox myDialogBox = new MyDialogBox("Impossibile inserire sottocategoria: sottocategoria gia' presente");
									}
								}
							});

						}
					});

					HorizontalPanel h3 = new HorizontalPanel();
					h3.add(tModificaCategoria);
					h3.add(bModificaCategoria);

					// modifica nome categoria
					bModificaCategoria.addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent eventClick) {

							final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
							greetingService.rinominaCategoria(event.getSelectedItem().getText(), tModificaCategoria.getValue(), new AsyncCallback<Boolean>() {

								@Override
								public void onFailure(Throwable caught) {
									MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
								}

								@Override
								public void onSuccess(Boolean result) {
									if(result) {
										final MyDialogBox myDialogBox = new MyDialogBox("Categoria rinominata con successo");
										vPanel.clear();
										GestioneCategorie_Page gc = new GestioneCategorie_Page(vPanel);
										gc.onModuleLoad();
									}
									else {
										final MyDialogBox myDialogBox = new MyDialogBox("Impossibile rinominare categoria");
									}
								}
							});
						}
					});
					HTML spazio = new HTML("</br>");
					vtemp.add(spazio);
					
					final Grid grid = new Grid(2,2);

					grid.setWidget(0, 0,  tAggiungiSottoCategoria);
					grid.setWidget(0, 1, bAggiungiSottoCategoria);

					vtemp.add(new HTML("<br><hr>"));
					vtemp.add(new HTML("<br/>"));
					vtemp.add(new HTML("<br/>"));

					vtemp.add(grid);
					
					final Grid grid2 = new Grid(2,2);

					grid2.setWidget(0, 0,  tModificaCategoria);
					grid2.setWidget(0, 1, bModificaCategoria);

					vtemp.add(new HTML("<br/>"));
					vtemp.add(new HTML("<br><hr>"));
					vtemp.add(new HTML("<br/>"));
					vtemp.add(new HTML("<br/>"));
					
					vtemp.add(grid2);
					
					vtemp.add(h2);
					vtemp.add(h3);

				}
			}
		});
		HorizontalPanel hp = new HorizontalPanel();
		hp.add(albero);
		vPanel.add(hp);
		vPanel.add(vtemp);

	}

	/**
	 * Metodo per la risoluzione dell'albero delle categorie
	 * 
	 * @param padre di tipo Categoria
	 * @param categorie ArrayList<Categoria> di categorie prensenti nel sistema
	 * @param nodoPadre albero
	 */
	private void getAlbero(Categoria padre, ArrayList<Categoria> categorie, TreeItem nodoPadre) {

		// set nodo padre
		if(padre.getNome().equals("null")) nodoPadre.setText("CATEGORIE DISPONIBILI");

		// set struttura albero
		for(Categoria c : categorie) {
			if(!listaCategorie.contains(c) && c.getPadre().getNome().equals(padre.getNome())) {
				TreeItem nodo = new TreeItem();
				nodo.setText(c.getNome());
				listaCategorie.add(c);

				for(Categoria sottoC : categorie) {
					if(sottoC.getPadre().getNome().equals(c.getNome())) {
						getAlbero(c, categorie, nodo);
					}
				}
				// aggiunge il nodo al nodo padre
				nodoPadre.addItem(nodo);
			}
		}
	}

}