package com.google.gwt.sample.sweng.client;

import com.google.gwt.core.client.EntryPoint;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe che definisce Members_Page
 * visualizza le informazioni relative ai componenti del gruppo di progetto
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class Members_Page implements EntryPoint {

	private VerticalPanel vPanel = null;

	/**
	 * Costruttore della classe Members_Page
	 * @param vp VerticalPanel
	 */
	public Members_Page( VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Caricamento Page
	 */
	@Override
	public void onModuleLoad() {

		HorizontalPanel hPanel = new HorizontalPanel();

		HTML titolo = new HTML("<h1> MEMBRI DEL PROGETTO </h1>");
		HTML descrizione = new HTML("<p> Questo sito e' stato realizzato da:<br><br>" + 
				"- Giacomo Rondelli<br>" + 
				"- Gabriele Malossi<br>" + 
				"- Giacomo Pazzaglia<br>");

		

		vPanel.add(titolo);
	
		hPanel.add(descrizione);

		vPanel.add(hPanel);

		
		
		hPanel.getElement().setAttribute("align", "center");
		vPanel.getElement().setAttribute("align", "center");

	}
}