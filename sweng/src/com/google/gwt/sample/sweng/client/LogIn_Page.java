package com.google.gwt.sample.sweng.client;

import com.google.gwt.core.client.GWT;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe che definisce LogIn_Page
 * permette di fare login nel sito di domande e risposte 
 *
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class LogIn_Page {

	private VerticalPanel vPanel = null;

	/**
	 * Costruttore dell classe LogIn_Page
	 * @param vp VerticalPanel
	 */
	public LogIn_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	public void onModuleLoad() {

		vPanel = new VerticalPanel();
		vPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

		HTML log_page = new HTML("<h1> Entra nel sito di domande e risposte! </h1>");
		vPanel.add(log_page);

		
		final Label label1 = new Label("Email: ");
		final TextBox textBoxUsername = new TextBox();
		final Label label2 = new Label("Password: ");
		final PasswordTextBox textBoxPassword = new PasswordTextBox();

		final Grid grid = new Grid(2, 2);

		grid.setWidget(0, 0,  label1);
		grid.setWidget(0, 1,  textBoxUsername);
		grid.setWidget(1, 0,  label2);
		grid.setWidget(1, 1,  textBoxPassword);

		final Button btnAvanti = new Button("Entra");
		btnAvanti.getElement().setAttribute("align", "center");

		btnAvanti.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

				greetingService.logIn(textBoxUsername.getText(), textBoxPassword.getText(), new AsyncCallback<Integer>() {

					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user
						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText(caught.getMessage());
					}

					@Override
					public void onSuccess(Integer result) {
						Account.email = (textBoxUsername.getText());
						Account.tipoAccount = result;

						vPanel.clear();

						//switch che mi fa entrare nel sito con privilegi diversi in base al tipo di profilo
						switch(result) {

						// password errata
						case -1:
							Sweng regTipo0 = new Sweng();
							regTipo0.onModuleLoad();
							MyDialogBox dialogBox2 = new MyDialogBox("Password errata riprovare!");
							break;

							// utente non registrato 
						case 0:
							MyDialogBox dialogBox1 = new MyDialogBox("Utente non registrato");
							break;

							// Giudice,
						case 1:
							Welcome_Page_Giudice regTipo1 = new Welcome_Page_Giudice(vPanel);
							regTipo1.onModuleLoad();
							break;

							// Amministratore
						case 2 :
							Welcome_Page_Amministratore regTipo2 = new Welcome_Page_Amministratore(vPanel);
							regTipo2.onModuleLoad();
							break;

							// Utente registrato
						case 3:
							Welcome_Page_Utente regTipo3 = new Welcome_Page_Utente(vPanel);
							regTipo3.onModuleLoad();
							break;

						default:
							System.out.println("[x]Errore interno!");
							break;

						}
					}
				});
			}
		});

	

		vPanel.add(new HTML("<br/>"));
		vPanel.add(new HTML("<br>"));
		vPanel.add(grid);
		vPanel.add(new HTML("<br>"));
		vPanel.add(btnAvanti);		
		vPanel.add(new HTML("<br/>"));
	

		Menu menu = new Menu( this.vPanel, 0);
		menu.onModuleLoad();

		vPanel.getElement().setAttribute("align", "center");

		RootPanel.get().add(vPanel);

	}


}
