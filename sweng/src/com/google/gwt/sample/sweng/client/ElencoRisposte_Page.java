package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.sample.sweng.shared.Domanda;
import com.google.gwt.sample.sweng.shared.Risposta;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * Classe che definisce ElencoRisposte_Page
 * Questa pagina memorizza le informazioni per la risposta ad una domanda
 * 
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class ElencoRisposte_Page {

	private VerticalPanel vPanel = null;
	private Domanda scelta;

	/**
	 * Costruttore dell classe ElencoRisposte_Page
	 * @param vp VerticalPanel
	 */
	public ElencoRisposte_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	public void onModuleLoad(Domanda scelta, String percorso) {

		vPanel.add(new HTML("</br>")); 	

		HTML titolo = new HTML("<h1> Dati domanda </h1>");
		vPanel.add(titolo);
		vPanel.add(new HTML("<br/>")); 	
		HTML stitolo = new HTML("<h4> PERCORSO: " + percorso +"</h4>");
		if (!percorso.equals("")) {
			vPanel.add(stitolo);
			vPanel.add(new HTML("<br/>")); 	
		}

		final Label flabelTesto = new Label("Testo:  ");
		final Label lTesto = new Label(scelta.getTesto());
		final Label flabelAutore = new Label("Autore: ");
		final Label lAutore = new Label(scelta.getNomeUtente());
		final Label flabelGiorno = new Label("Data: ");
		final Label lData = new Label(scelta.getDay());
		final Label flabelOra = new Label("Ora: ");
		final Label lOra = new Label(scelta.getTime());

		final Hyperlink hlink1 = new Hyperlink(scelta.getLink().get(0), scelta.getLink().get(0));
		final Hyperlink hlink2 = new Hyperlink(scelta.getLink().get(1), scelta.getLink().get(1));
		final Hyperlink hlink3 = new Hyperlink(scelta.getLink().get(2), scelta.getLink().get(2));
		final Hyperlink hlink4 = new Hyperlink(scelta.getLink().get(3), scelta.getLink().get(3));
		final Label flabelLink1 = new Label("Link1: ");
		final Label flabelLink2 = new Label("Link2: ");
		final Label flabelLink3 = new Label("Link3: ");
		final Label flabelLink4 = new Label("Link4: ");

		final Grid grid = new Grid(8,2);

		grid.setWidget(0, 0, flabelAutore);
		grid.setWidget(0, 1, lAutore);
		grid.setWidget(1, 0, flabelTesto);
		grid.setWidget(1, 1, lTesto);
		grid.setWidget(2, 0, flabelGiorno);
		grid.setWidget(2, 1, lData);
		grid.setWidget(3, 0, flabelOra);
		grid.setWidget(3, 1, lOra);
		grid.setWidget(4, 0, flabelLink1);
		grid.setWidget(4, 1, hlink1);
		grid.setWidget(5, 0, flabelLink2);
		grid.setWidget(5, 1, hlink2);
		grid.setWidget(6, 0, flabelLink3);
		grid.setWidget(6, 1, hlink3);
		grid.setWidget(7, 0, flabelLink4);
		grid.setWidget(7, 1, hlink4);
		vPanel.add(grid);
		vPanel.add(new HTML("</br>")); 	

		//Visualizzazione delle risposte relative alla domanda 
		final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		final int id = scelta.getId();

		greetingService.ordinaRisposte(id, new AsyncCallback<ArrayList<Risposta>>() {

			@Override
			public void onFailure(Throwable caught) {
				MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
			};

			@Override
			public void onSuccess(ArrayList<Risposta> response) {

				//set tabella di visualizzazione delle risposte alla domanda selezionata e delle relative colonne
				// colonna testo
				CellTable<Risposta> tabellaRisposte = new CellTable<Risposta>(50);

				TextColumn<Risposta> txtColTesto = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						return r.getTesto();
					}
				};
				tabellaRisposte.addColumn(txtColTesto, "RISPOSTA");

				//colonna autore
				TextColumn<Risposta> txtColAutore = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						return r.getNomeUtente();
					}
				};
				tabellaRisposte.addColumn(txtColAutore, "AUTORE");

				//colonna data
				TextColumn<Risposta> txtColData = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						return r.getDay();
					}
				};
				tabellaRisposte.addColumn(txtColData, "DATA");

				//colonna ora
				TextColumn<Risposta> txtColOra = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						return r.getTime();
					}
				};
				tabellaRisposte.addColumn(txtColOra, "ORARIO");

				//colonna nome giudice
				TextColumn<Risposta> txtColGiudice = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						if(r.getEmailGiudice().equals("")) return "N/A";
						else return r.getEmailGiudice();
					}
				};
				tabellaRisposte.addColumn(txtColGiudice, "GIUDICE");

				//colonna voto giudice
				TextColumn<Risposta> txtColVoto = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						if(r.getVoto().equals("")) return "N/A";
						else return r.getVoto();
					}
				};
				tabellaRisposte.addColumn(txtColVoto, "VOTO");

				txtColVoto.setSortable(true);

				//link 1
				TextColumn<Risposta> txtColLink1 = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						if(r.getLink().get(0).equals("")) return "N/A";
						else return r.getLink().get(0);
					}
				};
				tabellaRisposte.addColumn(txtColLink1, "LINK 1");

				//link 2
				TextColumn<Risposta> txtColLink2 = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						if(r.getLink().get(1).equals("")) return "N/A";
						else return r.getLink().get(1);
					}
				};
				tabellaRisposte.addColumn(txtColLink2, "LINK 2");

				//link 3
				TextColumn<Risposta> txtColLink3 = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						if(r.getLink().get(2).equals("")) return "N/A";
						else return r.getLink().get(2);
					}
				};
				tabellaRisposte.addColumn(txtColLink3, "LINK 3");

				//link 4
				TextColumn<Risposta> txtColLink4 = new TextColumn<Risposta>() {
					@Override
					public String getValue(Risposta r) {
						if(r.getLink().get(3).equals("")) return "N/A";
						else return r.getLink().get(3);
					}
				};
				tabellaRisposte.addColumn(txtColLink4, "LINK 4");

				// Create a data provider.
				ListDataProvider<Risposta> dataProvider = new ListDataProvider<Risposta>();

				// Connect the table to the data provider.
				dataProvider.addDataDisplay(tabellaRisposte);

				// Add the data to the data provider, which automatically pushes it to the
				// widget.
				List<Risposta> list = dataProvider.getList();
				for (Risposta r : response) {
					list.add(r);
				}

				// Add a ColumnSortEvent.ListHandler to connect sorting to the java.util.List.
				ListHandler<Risposta> columnSortHandler = new ListHandler<Risposta>(
						list);
				columnSortHandler.setComparator(txtColVoto,
						new Comparator<Risposta>() {
					public int compare(Risposta o1, Risposta o2) {
						if (o1.getVoto().equals(o2.getVoto())) {
							if (o1.getDate().after(o2.getDate())) return 1;
							else return -1;
						}
						if (o1 != null) {
							return (o2 != null) ? o1.getVoto().compareTo(o2.getVoto()) : 1;
						}
						return -1;

					}
				});
				tabellaRisposte.addColumnSortHandler(columnSortHandler);

				tabellaRisposte.getColumnSortList().push(txtColVoto);

				// set modalita' di selezione
				tabellaRisposte.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
				final SingleSelectionModel<Risposta> selectionModel = new SingleSelectionModel<Risposta>();
				tabellaRisposte.setSelectionModel(selectionModel);

				selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					boolean check = true;
					public void onSelectionChange(SelectionChangeEvent event) {		
						// possibilita' di inserire voto per il giudice
						if(Account.tipoAccount == 1) {
							Risposta scelta = selectionModel.getSelectedObject();
							
							if (scelta != null && check == true) {
								InserisciVoto_Page iv = new InserisciVoto_Page(vPanel);
								iv.onModuleLoad(scelta);	
							}
						}	
						check = false;
						System.out.println(check);
					}
				
				});

				tabellaRisposte.setRowCount(response.size(), true);
				tabellaRisposte.setRowData(0, response);
				vPanel.add(tabellaRisposte);	
			}

		});
	}

}
