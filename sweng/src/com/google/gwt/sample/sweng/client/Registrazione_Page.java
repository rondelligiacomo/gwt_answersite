package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import com.google.gwt.i18n.shared.DateTimeFormat;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

/**
 * Classe per la definizione della Registrazione_Page
 * registra le informazioni dell'utente nella fase di registrazione
 * @author Giacomo Rondelli, Gabriele Malossi
 */
public class Registrazione_Page {

	private VerticalPanel vPanel = null;

	/**
	 * Costruttore della classe Registrazione_Page
	 * @param vp VerticalPanel
	 */
	public Registrazione_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Caricamento Page
	 */
	public void onModuleLoad() {

		Menu menu = new Menu(this.vPanel, 0);
		menu.onModuleLoad();

		HTML registrazione_title = new HTML("<h1> Registrazione </h1>");
		vPanel.add(registrazione_title);

		HorizontalPanel hPanel = new HorizontalPanel();

		final Label label = new Label("* i campi contrassegnati sono obbligatori");
		hPanel.add(label);
		hPanel.add(new HTML("<br/>"));

		//dati anagrafica dell'utente
		final Label label1 = new Label("Username*: ");
		final TextBox textBoxUsername = new TextBox();
		final Label label2 = new Label("Password*: ");
		final TextBox textBoxPassword = new TextBox();
		final Label label3 = new Label("Email*: ");
		final TextBox textBoxEmail = new TextBox();
		final Label label4 = new Label("Nome: ");
		final TextBox textBoxNome = new TextBox();
		final Label label5 = new Label("Cognome: ");
		final TextBox textBoxCognome = new TextBox();
		final Label label6 = new Label("Sesso: ");
		final ListBox dropBoxSesso = new ListBox();
		dropBoxSesso.addItem("M");
		dropBoxSesso.addItem("F");
		final Label label7 = new Label("Data di nascita*: ");
		final DateBox textBoxDataNascita = new DateBox();
		final Label label8 = new Label("Luogo di nascita: ");
		final TextBox textBoxLuogoNascita = new TextBox();
		final Label label9 = new Label("Indirizzo: ");
		final TextBox textBoxIndirizzo = new TextBox();

		
		final ArrayList<Widget> listaWidgets = new ArrayList<Widget>();

		Collections.addAll(
				listaWidgets,
				label1, textBoxUsername
				,label2,textBoxPassword
				, label3, textBoxEmail
				, label4, textBoxNome
				, label5, textBoxCognome
				, label6, dropBoxSesso
				, label7, textBoxDataNascita
				, label8, textBoxLuogoNascita
				, label9, textBoxIndirizzo
				);

		final Grid grid = new Grid(9, 2);
		int posizione = 0;

		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 2; col++) {
				grid.setWidget(row, col, listaWidgets.get(posizione) );
				posizione++;
			}
		}

		
		final Button buttonInvio = new Button("Registrazione");
		buttonInvio.getElement().setAttribute("align", "center");

		buttonInvio.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
				Date data = textBoxDataNascita.getValue();					
				String dateString = dateFormat.format(data);

				ArrayList<String> listaDati = new ArrayList<String>();

				listaDati.add(textBoxUsername.getText());
				listaDati.add(textBoxPassword.getText());
				listaDati.add(textBoxEmail.getText());
				listaDati.add(textBoxNome.getText());
				listaDati.add(textBoxCognome.getText());
				listaDati.add(dropBoxSesso.getSelectedValue());
				listaDati.add(dateString);
				listaDati.add(textBoxLuogoNascita.getText());
				listaDati.add(textBoxIndirizzo.getText());

				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			
				
				greetingService.signUp(listaDati, new AsyncCallback<String>() {

					public void onFailure(Throwable caught) {
						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText("ERRORE!!"+ caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {

						if(result.equals("Registazione completata con successo")) {
							final MyDialogBox dialogBox = new MyDialogBox("Registrazione eseguita con successo");
							vPanel.clear();
							
							// login automatico dopo la registrazione
							Account.email = textBoxEmail.getText();
							Account.tipoAccount = 3;

							UtenteRegistrato_Page reg = new UtenteRegistrato_Page(vPanel);
							reg.onModuleLoad();
						}
						else {
							final MyDialogBox dialogBox = new MyDialogBox(result);
						}
					}
				});	
			}
		});

		vPanel.add(hPanel);
		vPanel.add(new HTML("<br/>"));
		vPanel.add(grid);


		vPanel.add(buttonInvio);

		hPanel.getElement().setAttribute("align", "center");
		vPanel.getElement().setAttribute("align", "center");
	}
}