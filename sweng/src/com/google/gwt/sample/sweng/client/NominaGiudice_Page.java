/**
 * Classe che definisce la nomina del giudice da parte di un amministratore
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
 

package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;


public class NominaGiudice_Page {

	private VerticalPanel vPanel = null;
	public ListBox lista = new ListBox();


	/**
	 * Costruttore della classe NominaGiudice_Page
	 * @param vp VerticalPanel
	 */
	public NominaGiudice_Page( VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Viene eseguito per primo: init()
	 */
	public void init(){
		getListaUtentiRegistrati();
	}

	/**
	 * Caricamento Page
	 */
	public void onModuleLoad() {

		init();

		Menu menu = new Menu(this.vPanel, 2);
		menu.onModuleLoad();

		HTML titolo = new HTML("<h1> Nomina Giudice </h1>");
		vPanel.add(titolo);
		vPanel.add(new HTML("</br>"));

		// specifica quali utenti possono essere promossi a giudici
		final Label label1 = new Label("Lista di Utente Registrati:");
		vPanel.add(label1);
		vPanel.add(lista);
		final Grid grid = new Grid(2,2);

		grid.setWidget(0, 0,  label1);
		grid.setWidget(0, 1, lista);

		HTML spazio = new HTML("</br>");

		vPanel.add(grid);
		vPanel.add(spazio);

		final Button buttonInvio = new Button("Avanti");

		buttonInvio.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String email = (lista.getItemText(lista.getSelectedIndex()).split(":"))[0].trim();

				try {
					final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
					greetingService.nominaGiudice(email, new AsyncCallback<String>() {

						public void onFailure(Throwable caught) {
							MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
						}

						@Override
						public void onSuccess(String response) {
							MyDialogBox myDialogBox = new MyDialogBox(response);
							lista.removeItem(lista.getSelectedIndex());
						}
					});
				} catch(Error e){};
			}
		});

		buttonInvio.getElement().setAttribute("align", "center");
		vPanel.add(buttonInvio);
	}

	/**
	 * Metodo per ottenere la lista degli utenti registrati presenti nel sistema
	 */

	public void getListaUtentiRegistrati() {
		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getUtenti(new AsyncCallback< ArrayList<String> >() {

				public void onFailure(Throwable caught) {
					MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
				}

				@Override
				public void onSuccess(ArrayList<String> response) {
					for(int i=0; i< response.size(); i++) {
						lista.addItem(response.get(i));
					}
				}
			});
		}catch(Error e){};
	}
}


