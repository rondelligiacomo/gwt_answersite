package com.google.gwt.sample.sweng.client;

import com.google.gwt.core.client.EntryPoint;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe che definisce Logout_Page
 * permette all'utente di uscire dal sito di domande e risposte
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class Logout_Page implements EntryPoint {

	
	private VerticalPanel vPanel = null;
	


	/**
	 * Caricamento Page
	 */
	@Override
	public void onModuleLoad() {
		RootPanel.get().clear();

		vPanel = new VerticalPanel();
		
		HTML title = new HTML("<h1> HAI EFFETTUATO IL LOGOUT </h1>");
		vPanel.add(title);
		
		
		Menu menu = new Menu( this.vPanel, 0);
		menu.onModuleLoad();

		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);

	}
}












