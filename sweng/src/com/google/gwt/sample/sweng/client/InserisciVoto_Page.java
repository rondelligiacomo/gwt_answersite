package com.google.gwt.sample.sweng.client;

import com.google.gwt.core.client.GWT;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sample.sweng.shared.Risposta;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe per la definizione della InserisciVoto_Page
 *
 *permette ad un giudice di votare una risposta di altri utenti 
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class InserisciVoto_Page {

	private VerticalPanel vPanel = null;

	/**
	 * Costruttore dell classe InserisciVoto_Page
	 * @param vp VerticalPanel
	 */
	public InserisciVoto_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	public void onModuleLoad(final Risposta scelta) {

		Menu menu = new Menu(this.vPanel, 1);
		menu.onModuleLoad();

		VerticalPanel vtemp = new VerticalPanel();

		HTML titolo = new HTML("<h1> Aggiungi voto </h1>");
		vtemp.add(titolo);
		vtemp.add(new HTML("</br>"));

		//imposto un voto che va da 0 a 5 
		final RadioButton rb0 = new RadioButton("myRadioGroup", "0");
		final RadioButton rb1 = new RadioButton("myRadioGroup", "1");
		final RadioButton rb2 = new RadioButton("myRadioGroup", "2");
		final RadioButton rb3 = new RadioButton("myRadioGroup", "3");
		final RadioButton rb4 = new RadioButton("myRadioGroup", "4");
		final RadioButton rb5 = new RadioButton("myRadioGroup", "5");
	

		vtemp.add(rb0);
		vtemp.add(rb1);
		vtemp.add(rb2);
		vtemp.add(rb3);
		vtemp.add(rb4);
		vtemp.add(rb5);
	


		final Grid grid = new Grid(1,6);

		grid.setWidget(0, 0, rb0);
		grid.setWidget(0, 1, rb1);
		grid.setWidget(0, 2, rb2);
		grid.setWidget(0, 3, rb3);
		grid.setWidget(0, 4, rb4);
		grid.setWidget(0, 5, rb5);

		rb2.setValue(true);

		HTML spazio = new HTML("</br>");

		vtemp.add(grid);
		vtemp.add(spazio);

		final Button buttonInvio = new Button("Inserisci voto");
		vtemp.add(buttonInvio);
		buttonInvio.getElement().setAttribute("align", "center");
		
		vPanel.add(vtemp);

		// inserimento voto giudice
		buttonInvio.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String voto = "";
				if (rb0.getValue()) voto = rb0.getText(); 
				if (rb1.getValue()) voto = rb1.getText();
				if (rb2.getValue()) voto = rb2.getText();
				if (rb3.getValue()) voto = rb3.getText();
				if (rb4.getValue()) voto = rb4.getText();
				if (rb5.getValue()) voto = rb5.getText();
				
				final Label label16 = new Label(voto);

				vPanel.add(label16);
				
				try {
					final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
					greetingService.inserisciVoto(scelta, Account.email, voto, new AsyncCallback<Boolean>() {

						public void onFailure(Throwable caught) {
							 MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
						}

						@Override
						public void onSuccess(Boolean response) {
							if(response) {
								MyDialogBox myDialogBox = new MyDialogBox("Voto inserito con successo");
								vPanel.clear();
								DomandeRisposte_Page domande_risposte = new DomandeRisposte_Page(vPanel);
								domande_risposte.onModuleLoad(null, "");
							}
							else {
								MyDialogBox myDialogBox = new MyDialogBox("Impossibile inserire voto");
								vPanel.clear();
								DomandeRisposte_Page domande_risposte = new DomandeRisposte_Page(vPanel);
								domande_risposte.onModuleLoad(null, "");
							}
						}
					});
				} catch(Error e){};
			}
		});
	}
}
