package com.google.gwt.sample.sweng.client;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe che definisce quali pagine aprire al click di un determinato tasto 
 * 
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

public class Comando {

	private VerticalPanel vPanel = null;

	/**
	 * Costruttore della classe Comando
	 * @param vp Vertical Panel
	 */
	public Comando(VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Comando della Page da caricare
	 * @param tipo Tipo di Panel da creare: info oppure logout oppure le altre voci di menu
	 * @return Oggetto contenente un Vertical Panel per il servizio richiesto
	 */


	public Command comando(String tipo) {
		Command cmd = null;

		if(tipo.equalsIgnoreCase("login")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					LogIn_Page login = new LogIn_Page(vPanel);
					login.onModuleLoad();
				}
			};
		}

		if(tipo.equalsIgnoreCase("registrazione")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					Registrazione_Page registrazione = new Registrazione_Page(vPanel);
					registrazione.onModuleLoad();
				}
			};
		}

		if(tipo.equalsIgnoreCase("info")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					Info_Page info = new Info_Page(vPanel);
					info.onModuleLoad();
				}
			};
		}
		
		if(tipo.equalsIgnoreCase("members")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					Members_Page members = new Members_Page(vPanel);
					members.onModuleLoad();
				}
			};
		}

		if(tipo.equalsIgnoreCase("domanda")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					InserisciDomanda_Page inserisci_domanda = new InserisciDomanda_Page(vPanel);
					inserisci_domanda.onModuleLoad();
				}
			};
		}

		if(tipo.equalsIgnoreCase("Elenco domande")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					DomandeRisposte_Page domande_risposte = new DomandeRisposte_Page(vPanel);
					domande_risposte.onModuleLoad(null, "");
				}
			};
		}

		
		if(tipo.equalsIgnoreCase("logout")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					Logout_Page logout = new Logout_Page();
					logout.onModuleLoad();
				}
			};
		}

		if(tipo.equalsIgnoreCase("Nomina giudice")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					NominaGiudice_Page nomina_giudice = new NominaGiudice_Page(vPanel);
					nomina_giudice.onModuleLoad();
				}
			};
		}

		if(tipo.equalsIgnoreCase("gestione categorie")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					GestioneCategorie_Page gestione_categorie = new GestioneCategorie_Page(vPanel);
					gestione_categorie.onModuleLoad();
				}
			};
		}

		if(tipo.equalsIgnoreCase("risposta")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					InserisciRisposta_Page inserisci_risposta = new InserisciRisposta_Page(vPanel);
					inserisci_risposta.onModuleLoad();
				}
			};
		}

		if(tipo.equalsIgnoreCase("elimina domanda")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					EliminaDomanda_Page elimina_domanda = new EliminaDomanda_Page(vPanel);
					elimina_domanda.onModuleLoad();
				}
			};
		}

		if(tipo.equalsIgnoreCase("elimina risposta")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();
					EliminaRisposta_Page elimina_risposta = new EliminaRisposta_Page(vPanel);
					elimina_risposta.onModuleLoad();
				}
			};
		}

		if(tipo.equalsIgnoreCase("Profilo")) {
			cmd = new Command() {
				public void execute() {
					vPanel.clear();

					switch(Account.tipoAccount) {
					case 0:
						Sweng regTipo0 = new Sweng ();
						regTipo0.onModuleLoad();
						break;

					case 1:
						Giudice_Page regTipo1 = new Giudice_Page(vPanel);
						regTipo1.onModuleLoad();
						break;

					case 2:
						Amministratore_Page regTipo2 = new Amministratore_Page(vPanel);
						regTipo2.onModuleLoad();
						break;

					case 3:
						UtenteRegistrato_Page regTipo3 = new UtenteRegistrato_Page(vPanel);
						regTipo3.onModuleLoad();
						break;

					default:
						System.out.println("ERROR");
						break;

					}
				}
			};
		}
		return cmd;
	}
}
