/**
 * 
 * Questa classe carica il profilo con i dati dell'utente di tipo utente registrato (normale)
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

package com.google.gwt.sample.sweng.client;




import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class UtenteRegistrato_Page implements EntryPoint{


	private VerticalPanel vPanel = null;

	/**
	 * Costruttore della classe UtenteRegistrato_Page
	 * @param vp VerticalPanel
	 */
	public UtenteRegistrato_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Caricamento Page
	 */
	public void onModuleLoad() {
		
		final HorizontalPanel hPanel = new HorizontalPanel();

		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getDatiUtente(Account.email, new AsyncCallback<String>() {

				public void onFailure(Throwable caught) {
					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText(caught.getMessage());
				}

				public void onSuccess(String info) {
					String [] arrayInfo = info.split("\n");
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					vPanel.add(new HTML("<br>"));
					String htmlProfilo = "<h1>Il tuo profilo</h1>"+
							arrayInfo[0]+"<br>"+
							arrayInfo[1]+"<br>"+
							arrayInfo[2]+"<br>"+
							arrayInfo[3]+"<br>"+
							arrayInfo[4]+"<br>"+
							arrayInfo[5]+"<br>"+
							arrayInfo[6]+"<br>"+
							arrayInfo[7]+"<br>"+
							"Tipo profilo : Utente registrato";



					hPanel.add(new HTML(htmlProfilo));


					vPanel.getElement().setAttribute("align", "center");
					vPanel.add(hPanel);
				}
			});
		}
		catch(Error e){};

		Menu menuUtente = new Menu(this.vPanel, Account.tipoAccount);
		menuUtente.onModuleLoad();

		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);
	}
}