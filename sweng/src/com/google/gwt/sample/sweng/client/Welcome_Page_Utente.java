

package com.google.gwt.sample.sweng.client;


import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe che definisce Welcome_Page_Utente
 * pagina di benvenuto al login dell'utente registrato normale
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

public class Welcome_Page_Utente {
	
	private VerticalPanel vPanel = null;

	/**
	 * Costruttore della classe Welcome_Page
	 * @param vp VerticalPanel
	 */
	public Welcome_Page_Utente( VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Caricamento Page
	 */
	public void onModuleLoad() {

		Menu menu = new Menu( this.vPanel, 3);
		menu.onModuleLoad();

		HTML titolo = new HTML("<h1> UTENTE BENVENUTO NEL SITO DI DOMANDE E RISPOSTE </h1>");

		vPanel.add(titolo);

	


		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);
	}
}


