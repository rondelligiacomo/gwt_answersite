

package com.google.gwt.sample.sweng.client;


import com.google.gwt.user.client.ui.HTML;

import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe che definisce Welcome_Page_Giudice
 * pagina di benvenuto al login del giudice
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

public class Welcome_Page_Giudice {
	
	private VerticalPanel vPanel = null;

	/**
	 * Costruttore della classe Welcome_Page
	 * @param vp VerticalPanel
	 */
	public Welcome_Page_Giudice( VerticalPanel vp) {
		this.vPanel = vp;
	}

	/**
	 * Caricamento Page
	 */
	public void onModuleLoad() {

		Menu menu = new Menu( this.vPanel, 1);
		menu.onModuleLoad();

		HTML titolo = new HTML("<h1> GIUDICE BENVENUTO NEL SITO DI DOMANDE E RISPOSTE </h1>");

		vPanel.add(titolo);

	


		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);
	}
}


