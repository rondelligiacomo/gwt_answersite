package com.google.gwt.sample.sweng.client;

import java.util.ArrayList;


import com.google.gwt.core.client.GWT;
import com.google.gwt.sample.sweng.shared.Domanda;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * Classe che definisce EliminaDomanda_Page
 * questa classe gestisce l'eliminazione di una domanda dall'elenco delle domande
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

public class EliminaDomanda_Page {

	private VerticalPanel vPanel = null;

	public EliminaDomanda_Page(VerticalPanel vp) {
		this.vPanel = vp;
	}

	public void onModuleLoad() {

		HTML titolo = new HTML("<h1> Elimina domanda </h1>");
		vPanel.add(titolo);
		HTML titolo2 = new HTML("<h2> Seleziona la domanda che vuoi eliminare</h2>");
		vPanel.add(titolo2);
		

		vPanel.add(new HTML("<br/>")); 	

		// lista delle domande
		final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		greetingService.getDomande(new AsyncCallback<ArrayList<Domanda>>() {

			@Override
			public void onFailure(Throwable caught) {
				MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
			};

			@Override
			public void onSuccess(ArrayList<Domanda> response) {

				// set tabella di visualizzazione delle domande e delle relative colonne
				
				// colonna testo
				CellTable<Domanda> tabellaDomande = new CellTable<Domanda>(50);

				TextColumn<Domanda> txtColTesto = new TextColumn<Domanda>() {
					@Override
					public String getValue(Domanda d) {
						return d.getTesto();
					}
				};
				tabellaDomande.addColumn(txtColTesto, "DOMANDA");

				// colonna autore
				TextColumn<Domanda> txtColAutore = new TextColumn<Domanda>() {
					@Override
					public String getValue(Domanda d) {
						return d.getNomeUtente();
					}
				};
				tabellaDomande.addColumn(txtColAutore, "AUTORE");

				// colonna data
				TextColumn<Domanda> txtColData = new TextColumn<Domanda>() {
					@Override
					public String getValue(Domanda d) {
						return d.getDay();
					}
				};
				tabellaDomande.addColumn(txtColData, "DATA");

				// colonna ora
				TextColumn<Domanda> txtColOra = new TextColumn<Domanda>() {
					@Override
					public String getValue(Domanda d) {
						return d.getTime();
					}
				};
				tabellaDomande.addColumn(txtColOra, "ORARIO");

				// set modalita' di selezione
				tabellaDomande.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
				final SingleSelectionModel<Domanda> selectionModel = new SingleSelectionModel<Domanda>();
				tabellaDomande.setSelectionModel(selectionModel);

				selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						Domanda scelta = selectionModel.getSelectedObject();
						if (scelta != null) {
							final int id = scelta.getId();
							try {
								greetingService.rimuoviDomanda(id, new AsyncCallback<Boolean>() {

									public void onFailure(Throwable caught) {
										MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());
									}

									@Override
									public void onSuccess(Boolean response) {
										if(response) {
											MyDialogBox myDialogBox = new MyDialogBox("La domanda e' stata eliminata correttamente");
											vPanel.clear();
											EliminaDomanda_Page ed = new EliminaDomanda_Page(vPanel);
											ed.onModuleLoad();
										}
										else {
											MyDialogBox myDialogBox = new MyDialogBox("ATTENZIONE ! Impossibile eliminare domanda");
										}
									}
								});
							} catch(Error e){};
						}
					}
				});

				tabellaDomande.setRowCount(response.size(), true);
				tabellaDomande.setRowData(0, response);
				vPanel.add(tabellaDomande);

			}
		});
	}
}
