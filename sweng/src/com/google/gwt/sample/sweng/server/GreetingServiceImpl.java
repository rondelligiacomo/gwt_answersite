package com.google.gwt.sample.sweng.server;

import java.util.ArrayList;
import java.util.Collection;
import com.google.gwt.sample.sweng.client.GreetingService;
import com.google.gwt.sample.sweng.shared.Domanda;
import com.google.gwt.sample.sweng.shared.Risposta;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.google.gwt.sample.sweng.server.UsersDB;
import com.google.gwt.sample.sweng.shared.Categoria;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {

	private GreetingService mockito = null;

	public void init() {
		UsersDB.initAmministratore();
		CategoriesDB.aggiungiCategoria(new Categoria("Ambiente"), null);
		CategoriesDB.aggiungiCategoria(new Categoria("Animali"), null);
		CategoriesDB.aggiungiCategoria(new Categoria ("Arte e cultura"), null);
		CategoriesDB.aggiungiCategoria(new Categoria ("Elettronica e tecnologia"), null);
		CategoriesDB.aggiungiCategoria(new Categoria ("Sport"), null);
		CategoriesDB.aggiungiCategoria(new Categoria ("Svago"), null);
	}

	// metodi per la gestione dell'utente
	@Override
	
	public String signUp(ArrayList<String> dati) throws IllegalArgumentException {
		return UsersDB.signUp(dati);
	}

	@Override
	public int logIn(String nomeUtente, String password) {
		return UsersDB.login(nomeUtente, password);
	}

	@Override
	public ArrayList<String> getUtenti() {
		return UsersDB.getlistaUtenti();
	}

	@Override
	public String getDatiUtente(String email) {
		return UsersDB.getInfoUtente(email);
	}

	// metodi per la gestione delle categorie

	@Override
	public Categoria getCategoria(int id) {
		return CategoriesDB.getCategoria(id);
	}

	@Override
	public ArrayList<Categoria> getAllCategorie() {
		return CategoriesDB.getCategorie();
	}

	@Override
	public boolean nuovaCategoria(Categoria c, String padre) {
		return CategoriesDB.aggiungiCategoria(c, padre);
	}

	@Override
	public boolean rinominaCategoria(String old, String newNome) {
		return CategoriesDB.modificaCategoria(old, newNome);
	}

	// metodi per la gestione delle domande
	@Override
	public ArrayList<Domanda> getDomande() {
		return QuestionsDB.visualizza();
	}

	@Override
	public boolean inserisciDomanda(Domanda d) {
		return QuestionsDB.aggiungi(d);
	}

	// ricordati che solo admin può usare questo metodo
	@Override
	public boolean rimuoviDomanda(int id) {
		return QuestionsDB.rimuovi(id);
	}

	// Metodi per la gestione delle risposte
	@Override
	public ArrayList<Risposta> getRisposte(int idDomanda) {
		return AnswersDB.visualizza(idDomanda);
	}

	@Override
	public ArrayList<Risposta> getTutteRisposte() {
		return AnswersDB.visualizzaTutte();
	}

	@Override
	public boolean inserisciRisposta(Risposta r) {
		return AnswersDB.aggiungi(r);
	}

	@Override
	public boolean rimuoviRisposta(int id) {
		return AnswersDB.rimuovi(id);
	}

	@Override
	public ArrayList<Risposta> ordinaRisposte(int idDomanda) {
		return AnswersDB.ordinamentoRisposte (idDomanda);
	}

	// metodi per la gestione dei giudici

	@Override
	public String nominaGiudice(String email) {
		return UsersDB.nominaGiudice(email);
	}

	// Metodi per la gestione dei voti
	@Override
	public boolean inserisciVoto(Risposta r, String emailG, String v) {
		return AnswersDB.aggiungiVoto(r, emailG, v);
	}

	// metodi per la gestione di mockito
	public GreetingService getMockito() {
		return mockito;
	}

	public void setMockito(GreetingService mockito) {
		this.mockito = mockito;
	}

}
