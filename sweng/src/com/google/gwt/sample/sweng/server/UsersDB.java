package com.google.gwt.sample.sweng.server;

import com.google.gwt.sample.sweng.shared.Utente;

import com.google.gwt.sample.sweng.shared.Amministratore;
import com.google.gwt.sample.sweng.shared.Giudice;


import java.io.File;
import java.util.ArrayList;
import java.util.Set;
import java.util.Map.Entry;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

/**
 * Classe che definisce un db per gli utenti
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 * 
 */

public class UsersDB {

	/**
	 * metodo get per ottenere il database delle risposte 
	 * @return db 
	 */
	private static DB getUsersDB() {
		DB db = DBMaker.newFileDB(new File("dbSwengProjectZ")).make();		
		return db;	
	}

	/**
	 * metodo per impostare l'amministratore
	 */
	public static void initAmministratore() {

		DB db = getUsersDB();
		BTreeMap<String, Utente> UtentiMap = db.getTreeMap("mappaUtenti");


		Amministratore a = new Amministratore("root", "root", "root@root.com", 
				"root", "root", "M", "Bologna", "10/07/1992", "Via Tosarelli 11");

		// Inserisco l'amministratore dentro al db
		UtentiMap.put(a.getEmail(), a);
		// scrivo e poi chiuso il db
		db.commit();
		db.close();
	}

	/**
	 * Metodo per la registrazione dell'utente
	 * @param dati dell'utente che effettua la registraione
	 * @return esito richiesta 
	 * @throws IllegalArgumentException
	 */
	
	public static String signUp(ArrayList<String> dati) {

		DB db = getUsersDB();
		BTreeMap<String, Utente> UtentiMap;

		if(validationUtente(dati)) {
			if(!checkEsistenza(dati.get(2))) {
				UtentiMap = db.getTreeMap("mappaUtenti");


				// Creazione utente 
				Utente u = new Utente(
						dati.get(0),
						dati.get(1),
						dati.get(2),
						dati.get(3),
						dati.get(4),
						dati.get(5),
						dati.get(6),
						dati.get(7),
						dati.get(8)
						
						);

				// Inserimento degli utenti all'interno del database
				UtentiMap.put(u.getEmail(), u);
				// Scrittura e chiusura del db
				db.commit();
				db.close();
				return "Registazione completata con successo";
			}
			else return "Indirizzo email gia' presente";
		}
		else return "Compilare tutti i campi";
	}

	/**
	 * Metodo per il controllo dei campi
	 * 
	 * @param dati inseriti nel form di registrazione
	 * @return true se i dati sono corretti, false altrimenti
	 */
	private static boolean validationUtente(ArrayList<String> dati) {
		for (int i=0; i<3; i++) {
			if(dati.get(i).isEmpty()|| dati.get(i).trim().length() <= 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Metodo che verifica l'esistenza di un utente
	 * 
	 * @param email utente di tipo String
	 * @return true se l'utente e' presente, false altrimenti
	 */
	private static boolean checkEsistenza(String email) {

		DB db = getUsersDB();
		BTreeMap<String, Utente> UtentiMap = db.getTreeMap("mappaUtenti");

		if (email.equalsIgnoreCase("admin@admin.com")) { 
			return true;
		} 
		else {
			for (Entry<String, Utente> entry : UtentiMap.entrySet()) {
				if(entry.getValue().getEmail().equalsIgnoreCase(email)) {
					// utente trovato
					return true;
				}
			}
		}
		// Utente non trovato 
		return false;
	}

	/**
	 * Metodo per effettuare il Login
	 * 
	 * @param nomeUtente, password di tipo String
	 * @return true se l'utente esiste e i parametri corrispondono, false altrimenti
	 */
	public static int login(String email, String password) throws IllegalArgumentException {

		DB db = getUsersDB();
		BTreeMap<String, Utente> UtentiMap = db.getTreeMap("mappaUtenti");

		if(checkEsistenza(email)) {
			Utente u = UtentiMap.get(email);

			// controllo password
			if(u.getPw().equals(password)) {
				// Utente trovato e dati di inserimento corretti
				if((u.getClass() == Giudice.class)) return  1;
				if((u.getClass() == Amministratore.class)) return 2;
				if((u.getClass() == Utente.class)) return 3;
			}
			else {
				return -1; //password errata
			}
		}
		return 0; // Utente non trovato

	}

	/**
	 * Metodo per visualizzare le informazioni dell'utente dopo che ha effettuato il login
	 * 
	 * @param email dell'utente di tipo String
	 * @return informazioni relative all'untente
	 */
	public static String getInfoUtente(String email) {

		DB db = getUsersDB();
		BTreeMap<String, Utente> UtentiMap = db.getTreeMap("mappaUtenti");

		Utente u = UtentiMap.get(email);

		String info = "Username :" + u.getNomeUtente() + "\nEmail : " + email + 
				"\nNome : " + u.getNome() + "\nCognome : " + u.getCognome()
				+ "\nSesso : " + u.getSesso() + "\nData Nascita : " + u.getDataNascita()
				+ "\nLuogo Nascita : " + u.getLuogoNascita() + "\nIndirizzo : " + u.getLuogo();


		return info;
	}

	/**
	 * Metodo per visualizzare la lista degli utenti iscritti alla piattaforma, non nominati giudici e non 
	 * amministratori
	 *
	 * @return lista utenti iscritti al sistema
	 */
	public static ArrayList<String> getlistaUtenti() {

		DB db = getUsersDB();
		BTreeMap<String, Utente> UtentiMap = db.getTreeMap("mappaUtenti");

		ArrayList<String> utenti = new ArrayList<String>();
		Set<String> keysU = UtentiMap.keySet(); 

		for (String key : keysU) {
			if(UtentiMap.get(key).getClass() == Utente.class)
				utenti.add(UtentiMap.get(key).getEmail());
		}

		return utenti;
	}

	/**
	 * Metodo che consente all'amministratore di nominare un giudice
	 * @param email dell'utente registrato
	 * @return esito della richiesta: true se effettuata con successo, false altrimenti
	 * @throws IllegalArgumentException
	 */
	public static String nominaGiudice(String email) {
		ArrayList<String> utentiEleggibili = getlistaUtenti();

		for (String utenteEleggibile : utentiEleggibili) {
			if (email.equalsIgnoreCase(utenteEleggibile)) {
				DB db = getUsersDB();
				BTreeMap<String, Utente> UtentiMap = db.getTreeMap("mappaUtenti");

				Utente u = UtentiMap.get(email);
				UtentiMap.put(email, new Giudice(u.getNomeUtente(), u.getPw(), u.getEmail(), u.getNome(), 
						u.getCognome(), u.getSesso(), u.getLuogoNascita(), u.getDataNascita(), u.getLuogo()));
				db.commit();
				db.close();
				return "Nomina eseguita con successo";
			}
		}
		return "Errore durante la nomina del giudice";
	}
}

