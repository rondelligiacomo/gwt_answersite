package com.google.gwt.sample.sweng.server;


import com.google.gwt.sample.sweng.shared.Risposta;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

/**
 * Classe per la definizione di un db per le risposte
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class AnswersDB {

	/**
	 * Metodo per ottenere il DB risposte
	 * @return db 
	 */
	private static DB getAnswersDB() {
		DB db = DBMaker.newFileDB(new File("dbSwengProjectZ")).make();		
		return db;	
	}

	/**
	 * Metodo per generare un Id
	 * 
	 * @return id da associare alla domanda
	 */
	public static int generateId() {

		DB db = getAnswersDB();
		BTreeMap<Integer,Risposta> RisposteMap = db.getTreeMap("mappaRisposte");

		if(RisposteMap.isEmpty()) return 0; 
		else return (RisposteMap.lastKey()+1);

	}

	/**
	 * Metodo per ottenere tutte le risposte relative a una specifica domanda
	 * 
	 * @param idDomanda di cui si vogliono visualizzare le risposte
	 * @return risposte ArrayList<Risposte> contentente tutte le risposte
	 */
	public static ArrayList<Risposta> visualizza(int idDomanda) {

		DB db = getAnswersDB();
		BTreeMap<Integer,Risposta> RisposteMap = db.getTreeMap("mappaRisposte");

		ArrayList<Risposta> risposte = new ArrayList<Risposta>();

		for(Map.Entry<Integer, Risposta> r : RisposteMap.entrySet()) {
			if(r.getValue().getIdDomanda() == idDomanda) {
				risposte.add(r.getValue());		
			}
		}
		db.commit();
		db.close();

		Collections.reverse(risposte);

		return risposte;
	}

	/**
	 * Metodo per ottenere tutte le risposte presenti nel sistema
	 * 
	 * @return risposte ArrayList<Risposte> contentente tutte le risposte
	 */
	public static ArrayList<Risposta> visualizzaTutte() {

		DB db = getAnswersDB();
		BTreeMap<Integer,Risposta> RisposteMap = db.getTreeMap("mappaRisposte");

		ArrayList<Risposta> risposte = new ArrayList<Risposta>();

		for(Map.Entry<Integer, Risposta> r : RisposteMap.entrySet()) {
			risposte.add(r.getValue());		
		}
		db.commit();
		db.close();

		Collections.reverse(risposte);

		return risposte;
	}

	/**
	 * Metodo per l'inserimento di una nuova risposta
	 * 
	 * @param oggetto r di tipo Risposta
	 * @return esito inserimento
	 */
	public static boolean aggiungi(Risposta r) {

		DB db = getAnswersDB();
		BTreeMap<Integer,Risposta> RisposteMap = db.getTreeMap("mappaRisposte");

		// controllo testo
		if(r.getTesto().trim().length()==0) return false;

		else {

			r.setId(generateId());

			RisposteMap.put(r.getId(), r);

			// Scrittura e chiusura del db
			db.commit();
			db.close();

			return true;
		}
	}

	/**
	 * Metodo per inserire un voto ad una risposta che non e' ancora stata votata
	 * 
	 * @param oggetto r di tipo Risposta
	 * @param emailGiudice di tipo String
	 * @param voto di tipo int
	 * @return true se l'operazione viene eseguita con successo,false altrimenti
	 */
	public static boolean aggiungiVoto(Risposta r, String emailG, String v) {
		DB db = getAnswersDB();
		BTreeMap<Integer,Risposta> RisposteMap = db.getTreeMap("mappaRisposte");

		if(r.getVoto().equals("") && !(r.getNomeUtente().equals(emailG))) {
			RisposteMap.put(r.getId(), 
					new Risposta(r.getId(), r.getIdDomanda(), r.getTesto(),r.getNomeUtente(), r.getLink(), emailG, v));
			db.commit();
			return true;
		} 
		else return false;
	}

	/**
	 * Metodo per eliminare una risposta
	 * 
	 * @param id della risposta da rimuovere
	 * @return esito operazione
	 */
	public static boolean rimuovi(int id) {

		DB db = getAnswersDB();
		BTreeMap<Integer, Risposta> RisposteMap = db.getTreeMap("mappaRisposte");

		// Rimozione della risposta dal database
		RisposteMap.remove(id);

		// Scrittura e chiusura del db
		db.commit();
		db.close();
		return true;
	}

	/**
	 * Metodo per l'ordinamento delle risposte
	 * 
	 * @param oggetto r di tipo Risposta
	 * @return 
	 * @return true se l'ordinamento avviene con successo
	 */
	public static ArrayList<Risposta> ordinamentoRisposte (int idDomanda) {

		DB db = getAnswersDB();
		BTreeMap<Integer,Risposta> RisposteMap = db.getTreeMap("mappaRisposte");

		ArrayList<Risposta> risposte = new ArrayList<Risposta>();
		ArrayList<Risposta> votate = new ArrayList<Risposta>();
		ArrayList<Risposta> nonVotate = new ArrayList<Risposta>();

		for(Entry<Integer, Risposta> r : RisposteMap.entrySet()) {
			if(r.getValue().getIdDomanda() == idDomanda) {
				if(r.getValue().getVoto() != "") {
					votate.add(r.getValue());
				}
				else nonVotate.add(r.getValue());
			}
		}

		Collections.reverse(nonVotate);
		Collections.reverse(votate);
		
		risposte.addAll(votate);
		risposte.addAll(nonVotate);
		
		db.commit();
		db.close();

		return risposte;
	}
}