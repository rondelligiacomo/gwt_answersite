package com.google.gwt.sample.sweng.server;

import java.io.File;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.sample.sweng.shared.Domanda;
import com.google.gwt.sample.sweng.shared.Risposta;

/**
 * Classe per la definizione di un db per le domande.
 * Una domanda puo' essere inserita, visualizzata, eliminata
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

public class QuestionsDB {

	/**
	 * Metodo per ottenere il db domande
	 * @return db 
	 */
	private static DB getQuestionsDB() {

		DB db = DBMaker.newFileDB(new File("dbSwengProjectZ")).make();		
		return db;	
	}
	
	/**
	 * Metodo per generare un Id
	 * 
	 * @return id da associare alla domanda
	 */
	private static int generateId() {

		DB db = getQuestionsDB();
		BTreeMap<Integer,Domanda> DomandeMap = db.getTreeMap("mappaDomande");
		
		if(DomandeMap.isEmpty()) return 0; 
		else return (DomandeMap.lastKey()+1);
		
	}


	/**
	 * Metodo per visualizzare tutte le domande presenti nel db
	 * 
	 * @return domande, ArrayString<Domanda> con tutte le domande presenti nel db
	 */
	public static ArrayList<Domanda> visualizza() {

		DB db = getQuestionsDB();
		BTreeMap<Integer,Domanda> DomandeMap = db.getTreeMap("mappaDomande");

		ArrayList<Domanda> domande = new ArrayList<Domanda>();

		if(!DomandeMap.isEmpty()) {
			for(Map.Entry<Integer, Domanda> d : DomandeMap.entrySet()) {
				domande.add(d.getValue());
			}
		}
		Collections.reverse(domande);
		
		return domande;
	}

	/**
	 * Metodo per inserere una  nuova domanda
	 * 
	 * @param Domanda d
	 * @return esito inserimento
	 */
	public static boolean aggiungi(Domanda d) {

		DB db = getQuestionsDB();
		BTreeMap<Integer,Domanda> DomandeMap = db.getTreeMap("mappaDomande");

		// controllo testo
		if(d.getTesto().trim().length()==0) return false;

		else {
			
			int id = generateId();
			d.setId(id);
			
			DomandeMap.put(d.getId(), d);

			// Scrittura e chiusura del db
			db.commit();
			db.close();

			return true;
		}

	}

	/**
	 * Metodo per eliminare una domanda
	 * 
	 * @param id della domanda da rimuovere
	 * @return true se domanda rimossa, false altrimenti
	 */
	public static boolean rimuovi(int id) {

		if(checkDomanda(id)) {
			DB db = getQuestionsDB();
			BTreeMap<Integer,Domanda> DomandeMap = db.getTreeMap("mappaDomande");
			BTreeMap<Integer, Risposta> RisposteMap = db.getTreeMap("mappaRisposte");

			// da valutare successivamente se richiamare il metodo
			for(Entry<Integer, Risposta> entry : RisposteMap.entrySet()) {

				// Cerca l'id della domanda nelle risposte
				if(entry.getValue().getIdDomanda() == id) {
					RisposteMap.remove(entry.getKey());
				}
			}
			
			// Rimozione della domanda dal database
			DomandeMap.remove(id);
			
			// Scrittura e chiusura del db
			db.commit();
			db.close();
			return true;
		} 
		else {
			return false;
		}
	}

	/**
	 * Metodo che verifica l'esistenza di una domanda
	 * 
	 * @param id domanda di tipo int
	 * @return true se l'id domanda e' presente,
	 * 	false altrimenti
	 */
	private static boolean checkDomanda(int id) {

		DB db = getQuestionsDB();
		BTreeMap<Integer,Domanda> DomandeMap = db.getTreeMap("mappaDomande");

		if(DomandeMap.containsKey(id)) return true;
		else return false;
	}

}
