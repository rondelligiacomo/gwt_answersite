package com.google.gwt.sample.sweng.server;
import com.google.gwt.sample.sweng.shared.Categoria;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

/**
 * Classe per la definizione di un DB per le categorie. 
 * 
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 * 
 */

public class CategoriesDB {

	/**
	 * Metodo per ottenere il db categorie
	 * @return db 
	 */
	private static DB getCategoriesDB() {

		DB db = DBMaker.newFileDB(new File("dbSwengProjectZ")).make();		
		return db;	
	}


	/**
	 * Metodo per ottenere una categoria associata ad un id
	 * @param idCategoria della categoria desiderata
	 * @return categoria se esiste, null altrimenti
	 */
	public static Categoria getCategoria(int id) {

		DB db = getCategoriesDB();
		BTreeMap<Integer,Categoria> categorieMap = db.getTreeMap("MapCategorie");
		// questo metodo ritorna la categoria associata all'id se questa esiste, null altrimenti
		Categoria categoria = categorieMap.getOrDefault(id, null);
		return categoria;
	}

	/**
	 * Metodo per ottnere la lista delle categorie presenti nel sistema
	 * @return categorie, ArraList<Categoria> contente le categorie
	 */
	public static ArrayList<Categoria> getCategorie(){

		DB db = getCategoriesDB();
		BTreeMap<Integer, Categoria> categorieMap = db.getTreeMap("MapCategorie");

		ArrayList<Categoria> categorie = new ArrayList<Categoria>();
		Categoria root = new Categoria("ROOT");
		if(!categorieMap.isEmpty()) {
			for(Map.Entry<Integer, Categoria> c : categorieMap.entrySet()) {
				categorie.add(c.getValue());		
			}
		}
		return categorie;		
	}

	/**
	 * Metodo che verifica l'esistenza di una categoria
	 * 
	 * @param nome Categoria di tipo String
	 * @return true se la categoria e' presente, false altrimenti
	 */
	private static boolean checkCategoria(String nome) {

		DB db = getCategoriesDB();
		BTreeMap<Integer, Categoria> categorieMap = db.getTreeMap("MapCategorie");

		if(categorieMap.isEmpty()) return false; 
		else {
			for (Entry<Integer, Categoria> entry : categorieMap.entrySet()) {
				if(entry.getValue().getNome().equalsIgnoreCase(nome)) return true;
			}
		}
		//se la categoria non esiste
		return false;
	}

	/**
	 * Metodo per aggiungere una nuova categoria (base o sottocategoria)
	 * 
	 * @param c di tipo Categoria
	 * @param padre di tipo String
	 * @return true se inserimento con successo, false altrimenti
	 */
	public static boolean aggiungiCategoria(Categoria c, String padre) {

		DB db = getCategoriesDB();
		BTreeMap<Integer, Categoria> categorieMap = db.getTreeMap("MapCategorie");
		boolean aggiungi= true;

		// se la categoria esiste, ritorna false perche non e' possibile aggiungerla 
		if(checkCategoria(c.getNome()) || c.getNome().trim().length()==0) return false; 
		else {
			if (aggiungi) {
				int id = (categorieMap.size()+1);
				boolean catPadreCheck = false;
				Categoria cPadre = null;
				c.setId(id);

				if(padre==null) {
					cPadre = new Categoria("null");
					catPadreCheck = true;
				} else {
					if(!categorieMap.isEmpty()) {
						for(Map.Entry<Integer, Categoria> categorie: categorieMap.entrySet()) {
							if(categorie.getValue().getNome().equals(padre)) {
								catPadreCheck = true;
								cPadre = categorie.getValue();
							}
						}
					}
				}
				if(catPadreCheck) {
					c.setPadre(cPadre);
					categorieMap.put(id,c);
					if(cPadre != null) {
						for(Map.Entry<Integer, Categoria> categorie: categorieMap.entrySet()) {
							if(categorie.getValue().getNome().equals(padre)) {
								Categoria p = categorie.getValue();
								p.setCategoriaFiglia(c);

								categorieMap.remove(p.getId());
								categorieMap.put(p.getId(), p);
							}
						}
					}
				} else {
					db.commit();
					return false;
				}
				db.commit();
				return true; 	
			}
			else {
				db.commit();
				return false;
			}
		}
	}


	/**
	 * Metodo per modificare il nome di una categoria
	 * @param nomeCategoria vecchio
	 * @param nomeNuovo nuovo
	 * @return true se operazione con successo, false altrimenti
	 */
	public static boolean modificaCategoria(String oldNome, String newNome) {
		DB db = getCategoriesDB();
		BTreeMap<Integer,Categoria> categorieMap = db.getTreeMap("MapCategorie");
		boolean result = false;

		if (newNome.trim().length()==0) return result;

		if (!categorieMap.isEmpty()) {
			for(Map.Entry<Integer, Categoria> c : categorieMap.entrySet()) {
				if(c.getValue().getNome().contentEquals(oldNome)) {
					// creazione di una categoria temporanea per la modifica del nome
					Categoria temp = c.getValue();
					temp.setNome(newNome);;
					categorieMap.remove(temp.getId());
					categorieMap.put(temp.getId(), temp);
					result = true;
				}
			}
		}
		db.commit();
		db.close();
		return result;		
	}

}