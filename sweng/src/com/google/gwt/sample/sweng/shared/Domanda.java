package com.google.gwt.sample.sweng.shared;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.i18n.shared.DateTimeFormat;

/**
 * Classe per la definizione di una domanda
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

public class Domanda implements Serializable { 

	private static final long serialVersionUID = 1L; 
	
	private int id;
	private String testo;
	private String nomeUtente;
	private String nomeCategoria;
	private Date date;
	private ArrayList<String> link; 

	
	/**
	 * Costruttore
	 */
	public Domanda() {
		
	}
	
	/**
	 * Costruttore
	 * @param testo di tipo String
	 * @param nomeCategoria di tipo String
	 * @param nomeUtente di tipo String
	 * @param link di tipo ArrayList<String>
	 * @param date di tipo Date
	 */
	public Domanda(int id, String testo, String nomeCategoria, String nomeUtente, ArrayList<String> link) {
		this.id = id;
		this.testo = testo;
		this.nomeCategoria = nomeCategoria;
		this.nomeUtente = nomeUtente;
		this.date = new Date();
		this.link = link;
	}

	/**
	 * Estrattore id domanda
	 * @return id di tipo int
	 */
	public int getId(){
		return this.id;
	}
	
	/**
	 * Set id domanda
	 * @return id di tipo int
	 */
	public void setId(int nId){
		this.id = nId;
	}

	/**
	 * Estrattore testo domanda
	 * @return testo di tipo String
	 */
	public String getTesto(){
		return this.testo;
	}
	
	/**
	 * Estrattore nomeCategoria domanda
	 * @return nomeCategoria di tipo String
	 */
	public String getCategoria(){
		return this.nomeCategoria;
	}

	/**
	 * Estrattore nome utente
	 * @return nome utente di tipo String
	 */
	public String getNomeUtente(){
		return this.nomeUtente;
	}

	/**
	 * Estrattore del link
	 * @return link di tipo String
	 */
	public ArrayList<String> getLink() {
		return this.link;
	}

	/**
	 * Estrattore data
	 * @return date di tipo Date
	 */
	public Date getDate(){
		return date;
	}

	/**
	 * Estrattore giorno
	 * @return day formato dd-mm-yyyy di tipo String
	 */
	public String getDay(){
		return DateTimeFormat.getFormat("dd-MM-yyyy").format(this.date);
	}

	/**
	 * Estrattore ora
	 * @return time formato hh:mm:ss di tipo String
	 */

	public String getTime(){	
		return DateTimeFormat.getFormat("hh:mm:ss").format(this.date);
	}
}