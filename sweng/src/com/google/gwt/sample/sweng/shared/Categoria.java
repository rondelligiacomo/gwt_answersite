package com.google.gwt.sample.sweng.shared;
import java.io.Serializable;

import java.util.ArrayList;

/**
 * Classe per la definizione di una categoria
 * 
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */

public class Categoria implements Serializable { 

	private static final long serialVersionUID = 1L;

	private String nome;
	private Categoria padre;
	private ArrayList<Categoria> categorieFiglie;
	private int id;

	/**
	 * Costruttore
	 */
	public Categoria() {

	} 

	/**
	 * Costruttore
	 * 
	 * @param nome categoria di tipo String
	 */
	public Categoria(String nome) {
		categorieFiglie = new ArrayList<Categoria>();
		this.nome = nome;
	}

	/**
	 * Estrattore id categoria
	 * @return id categoria di tipo int
	 */
	public int getId(){
		return this.id;
	} 

	/**
	 * Set id categoria
	 * @param id categoria
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Estrattore nome categoria
	 * @return nome  di tipo String
	 */
	public String getNome(){
		return this.nome;
	}

	/**
	 * Estrattore padre categoria
	 * @return padre categoria di tipo Categoria
	 */
	public Categoria getPadre(){
		return this.padre;
	} 

	/**
	 * Set padre categoria
	 * @param padre categoria di tipo Categoria
	 */
	public void setPadre(Categoria padre) {
		this.padre = padre;
	}

	/*
	 * Set nome categoria
	 * @param nome categoria di tipo String
	 */
	public void setNome(String nuovo){
		nome = nuovo;
	} 

	/**
	 * Estrattore categorie figlie 
	 * @return ArrayList contente le categorie figlie
	 */
	public ArrayList<Categoria> getCategorieFiglie() {
		return categorieFiglie;
	}

	/**
	 * Set categoria figlia
	 * @param categoria figlia di tipo Categoria
	 */
	public void setCategoriaFiglia(Categoria categoria) {
		this.categorieFiglie.add(categoria);
	}

	/**
	 * Set lista di categorie figlie
	 * @param categorieFiglie di tipo ArrayList<Categoria>
	 */
	public void setCategorieFiglie(ArrayList<Categoria> categorieFiglie) {
		this.categorieFiglie = categorieFiglie;
	}

	@Override
	public String toString() {
		return "\nID: " + this.id + "\nNome: " + this.nome + "\nPadre: " + this.padre +"\n";
	}

}


