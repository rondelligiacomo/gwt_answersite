package com.google.gwt.sample.sweng.shared;

import java.io.Serializable;

/**
 * Classe per la definizione di una giudizio
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */


@SuppressWarnings("serial")

public class Giudizio implements Serializable { 
	


	private String nomeUtente; 
	private int idRisposta;
	private int voto;
	
	public Giudizio() {
	} 
	
	/**
	 * Costruttore
	 * @param nomeUtente di tipo String
	 * @param id di tipo int
	 * @param voto di tipo int
	 */
	public Giudizio(String nomeUtente, int id, int voto) {
		this.nomeUtente = nomeUtente;
		this.idRisposta = id;
		this.voto = voto;
	}
	
	/**
	 * Estrattore nome utente
	 * @return nome utente di tipo String
	 */
	public String getNomeUtente(){
		return this.nomeUtente;
	}
	
	/**
	 * Estrattore id risposta 
	 * @return id di tipo int
	 */
	public int getId(){
		return this.idRisposta;
	}
	
	/**
	 * Estrattore voto 
	 * @return voto di tipo int
	 */
	public int getVoto(){
		return this.voto;
	}
}

