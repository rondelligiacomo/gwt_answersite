package com.google.gwt.sample.sweng.shared;
import java.io.Serializable;

import com.google.gwt.i18n.shared.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;

/**
 * Classe per la definizione di una risposta
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */



public class Risposta implements Serializable { 

	private static final long serialVersionUID = 1L;

	private int id;
	private int idDomanda;
	private String testo;
	private String nomeUtente;
	private Date date;
	private String emailGiudice;
	private String voto;
	private ArrayList<String> link; 

	/**
	 * Costruttore
	 */
	public Risposta() {	

	}

	/**
	 * Costruttore 
	 * @param idDomanda di tipo int
	 * @param testo di tipo String
	 * @param nomeUtente di tipo String
	 * @param link di tipo ArrayList<link>
	 */
	public Risposta(int id, int idDomanda, String testo, String nomeUtente, ArrayList<String> link) {
		this.id = id;
		this.idDomanda = idDomanda;
		this.testo = testo;
		this.nomeUtente = nomeUtente;
		this.link = link;
		this.date = new Date();
		this.emailGiudice = "";
		this.voto = "";
	}

	/**
	 * Costruttore
	 * @param idDomanda di tipo Int
	 * @param id di tipo Int
	 * @param testo di tipo String
	 * @param nomeUtente di tipo String
	 * @param link di tipo  ArrayList<String>
	 * @param date di tipo Date
	 * @param emailGiudice di tipo String
	 * @param voto di tipo int
	 */
	public Risposta(int id, int idDomanda, String testo, String nomeUtente, ArrayList<String> link, String emailGiudice, String voto) {
		this.id = id;
		this.idDomanda = idDomanda;
		this.testo = testo;
		this.nomeUtente = nomeUtente;
		this.link = link;
		this.date = new Date();
		this.emailGiudice = emailGiudice;
		this.voto = voto;
	}

	/**
	 * Estrattore id risposta 
	 * @return id di tipo int
	 */
	public int getId(){
		return this.id;
	}

	/**
	 * Set id domanda
	 * @return id di tipo int
	 */
	public void setId(int nId){
		this.id = nId;
	}

	/**
	 * Estrattore idDomanda a cui una risposta fa riferimento 
	 * @return idDomanda di tipo int
	 */
	public int getIdDomanda(){
		return this.idDomanda;
	}

	/**
	 * Estrattore testo risposta
	 * @return testo di tipo String
	 */
	public String getTesto(){
		return this.testo;
	}

	/**
	 * Estrattore nome utente
	 * @return nome utente di tipo String
	 */
	public String getNomeUtente(){
		return this.nomeUtente;
	}

	/**
	 * Estrattore del link
	 * @return link di tipo String
	 */
	public ArrayList<String> getLink() {
		return this.link;
	}

	/**
	 * Estrattore data
	 * @return date di tipo Date
	 */
	public Date getDate(){
		return date;
	}

	/**
	 * Estrattore giorno
	 * @return day formato dd-mm-yyyy di tipo String
	 */
	public String getDay(){
		return DateTimeFormat.getFormat("dd-MM-yyyy").format(this.date);
	}

	/**
	 * Estrattore ora
	 * @return time formato hh:mm:ss di tipo String
	 */

	public String getTime(){	
		return DateTimeFormat.getFormat("hh:mm:ss").format(this.date);
	}

	/**
	 * Estrattore emailUtente
	 * @return emailUtente di tipo String
	 */
	public String getEmailGiudice(){
		return this.emailGiudice;
	}

	/**
	 * Estrattore voto 
	 * @return voto di tipo int
	 */
	public String getVoto(){
		return this.voto;
	}

}