package com.google.gwt.sample.sweng.shared;
import java.io.Serializable;


/**
 * Classe che definisce il giudice
 *
 * @author Giacomo Rondelli, Gabriele Malossi, Giacomo Pazzaglia
 */
public class Giudice extends Utente implements Serializable  {

	private static final long serialVersionUID = 1L;

	/**
	 * Costruttore di default
	 */
	public Giudice(){

	}

	/**
	 * Costruttore 
	 * 
	 * @param nomeUtente di tipo String
	 * @param password di tipo String
	 * @param email di tipo String
	 * @param nome di tipo String
	 * @param cognome di tipo String
	 * @param sesso di tipo String
	 * @param luogoNascita di tipo String
	 * @param dataNascita di tipo String
	 * @param luogoDomRes di tipo String 

	 */
	public Giudice(String nomeUtente, String password, String email, String nome, String cognome, 
			String sesso, String dataNascita, String luogoNascita, String luogoDomRes) {
		super(nomeUtente, password, email, nome, cognome, sesso, dataNascita, luogoNascita, luogoDomRes);
	}


}